# UNPR

An adaptation of Santiago Montesdeoca MNPR for Autodesk Maya to Unity

> Warning : This project is still a work in progress, the base code is working but not yet optimised (and it could be cleaner too) and MNPR tools have not yet been recreated.

***

## Description

UNPR stands for Unity Non Photorealistic Rendering. It is an adapatation of Santiago E. Montesdeoca MNPR (Maya Non Photorealistic Rendering) for Unity. This adaptation is done using a custom SRP (Scriptable render pipeline) and thus is not compatible with Unity legacy/URP/HDRP shaders.


## Installation

This repository is simply a Unity project (version 2020.3.1f1). Add it directly in Unity Hub and you'll be able to launch it.


## TODOs

Here is a quick list of things I plan to do on this project in the future:
- Recreate MNPR Tools (effect painting on mesh)
- Optimisation and cleanup :
  - Rework the Post-Processing parameters editor (use serializable classes instead of a fully custom editor)
  - Optimisation of the post processing and cleaner debug tool/frame debugger integration
- More shaders (firstly a Lit one would be nice) and example materials
- A material update tool to adapt common material to UNPR specific Scriptable Rendering Pipeline


## Links

MNPR : 
- <https://artineering.io/software/Maya-NPR/>
- <https://mnpr.artineering.io/>
- <https://github.com/semontesdeoca/MNPR>

Santiago E. Montedescoa thesis : <https://dr.ntu.edu.sg/handle/10220/47356>