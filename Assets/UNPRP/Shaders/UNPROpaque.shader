﻿Shader "UNPR/Opaque"
{
    Properties
    {
        _BaseColor("Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _MainTex("Albedo", 2D) = "white" {}

        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("Src Blend", Float) = 1
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("Dst Blend", Float) = 0
        [Enum(Off, 0, On, 1)] _ZWrite("Z Write", Float) = 1
    }

    SubShader
    {
        //Cull Off ZWrite Off ZTest Always

            Pass
        {
            //Name ""
            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            HLSLPROGRAM

                #include "UNPROpaquePass.hlsl"
                #pragma vertex UNPROpaqueVertex
                #pragma fragment UNPROpaqueFragment

            ENDHLSL
        }
    }
}
