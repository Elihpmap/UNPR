﻿Shader "Hidden/UNPR/MRTTestShader"
{
    HLSLINCLUDE

        #include "../../ShaderLibrary/Common.hlsl"

        //given parameters (given in MNPRcommon)
        //TEXTURE2D(_MainTex);
        //SAMPLER(sampler_MainTex);

        //struct 
        struct Varyings{
            float4 positionCS : SV_POSITION;
            float2 screenUV : VAR_SCREEN_UV;
        };
        struct mrt2 {
            float4 target0 : SV_TARGET0;
            float4 target1 : SV_TARGET1;
        };

        //vertex

        Varyings objectVertex(
            float4 vertex : POSITION, // vertex position input
            float2 uv : TEXCOORD0 // first texture coordinate input
        )
        {
            Varyings o;
            float3 positionWS = TransformObjectToWorld(vertex);
            o.positionCS = TransformWorldToHClip(positionWS);
            o.screenUV = uv;
            return o;
        }

        Varyings clipVertex(
                float4 vertex : POSITION, // vertex position input
                float2 uv : TEXCOORD0 // first texture coordinate input
            )
        {
            Varyings o;
            /*
            float3 positionWS = TransformObjectToWorld(vertex);
            o.positionCS = TransformWorldToHClip(positionWS);
            */
            o.positionCS = vertex;
            o.screenUV = uv;
            return o;
        }

        //fragment
        float4 copyFrag(Varyings i) : SV_Target
        {
            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.screenUV);
            //float4 color = float4(0, 1, 1, 1);
            return color;
        }

        mrt2 testFrag(Varyings i)
        {
            mrt2 o;
            o.target0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.screenUV);
            o.target1 = (i.screenUV.x > 0 && i.screenUV.x < 1 &&
                         i.screenUV.y > 0 && i.screenUV.y < 1) ? float4(i.screenUV, 0, 1) : float4(0, 0, 1, 1);
            return o;
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            Name "objectPass"
            HLSLPROGRAM

                #pragma vertex objectVertex
                #pragma fragment testFrag

            ENDHLSL
        }    
        
        Pass
        {
            Name "clipPass"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment testFrag

            ENDHLSL
        }
    }
}
