////////////////////////////////////////////////////////////////////////////////////////////////////
// quadGapsOverlaps10.fx (HLSL)
// Brief: Creating gaps and overlaps at the edges of the rendered image
// Contributors: Santiago Montesdeoca, Pierre B�nard
////////////////////////////////////////////////////////////////////////////////////////////////////
//                             _                        _                 
//     __ _  __ _ _ __  ___   | |    _____   _____ _ __| | __ _ _ __  ___ 
//    / _` |/ _` | '_ \/ __|  | |   / _ \ \ / / _ \ '__| |/ _` | '_ \/ __|
//   | (_| | (_| | |_) \__ \  | |  | (_) \ V /  __/ |  | | (_| | |_) \__ \
//    \__, |\__,_| .__/|___/  | |   \___/ \_/ \___|_|  |_|\__,_| .__/|___/
//    |___/      |_|          |_|                              |_|        
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader provides the algorithms to produce gaps and overlaps commonly found in illustrations
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"
#include "ColorTransform.hlsl"


// TEXTURES
TEXTURE2D_SAMPLER2D (_EdgeTex, sampler_EdgeTex);
Texture2D _EdgeCtrlTex;
Texture2D _BlendingTex;


// VARIABLES
float3 _SubstrateColor = float3(1.0, 1.0, 1.0);
float _GapsOverlapsWidth = 5;

// MRT // TODO why? there's only on render target 
struct fragmentOutput {
    float4 colorOutput : SV_Target0;
    float alphaOutput : SV_Target1;
};



//     __                  _   _
//    / _|_   _ _ __   ___| |_(_) ___  _ __  ___
//   | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
//   |  _| |_| | | | | (__| |_| | (_) | | | \__ \
//   |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
//

// Contributor: Pierre B�nard
// rgb (red, green, blue) to ryb (red, yellow, blue) color space transformation
// -> Based on color mixing model by Chen et al. 2015
//    [2015] Wetbrush: GPU-based 3D Painting Simulation at the Bristle Level
float3 mixRYB2(float3 color1, float3 color2) {
    float3x3 M = float3x3(0.241, 0, 0, 0, 0.691, 0, 0, 0, 0.068);  // luminance matrix
    // measure RGB brightness of colors
    float b1 = luminance(color1);
    float b2 = luminance(color2);
    //float b1 = pow(dot(color1, mul(M, color1)), 0.5);
    //float b2 = pow(dot(color2, mul(M, color2)), 0.5);
    float bAvg = 0.5*(b1 + b2);
    // convert colors to RYB
    float3 ryb1 = rgb2ryb(color1);
    float3 ryb2 = rgb2ryb(color2);
    // mix colors in RYB space
    float3 rybOut = 0.5*(ryb1 + ryb2);
    // bring back to RGB to measure brightness
    float3 rgbOut = ryb2rgb(rybOut);
    // measure brightness of result
    //float b3 = pow(dot(rgbOut, mul(M, rgbOut)),0.5);
    float b3 = luminance(rgbOut);
    //modify ryb with brightness difference
    rybOut *= (bAvg / b3) * 0.9;  // reduced the brightness a bit

    //convert and send back
    return ryb2rgb(rybOut);
}



//                               ___                           _                 
//     __ _  __ _ _ __  ___     ( _ )       _____   _____ _ __| | __ _ _ __  ___ 
//    / _` |/ _` | '_ \/ __|    / _ \/\    / _ \ \ / / _ \ '__| |/ _` | '_ \/ __|
//   | (_| | (_| | |_) \__ \   | (_>  <   | (_) \ V /  __/ |  | | (_| | |_) \__ \
//    \__, |\__,_| .__/|___/    \___/\/    \___/ \_/ \___|_|  |_|\__,_| .__/|___/
//    |___/      |_|                                                  |_|        

// Contributor: Santiago Montesdeoca
// Creates the gaps and overlaps for sketchy illustrated rendering
// -> Based on the gaps & overlaps model by Montesdeoca et al. 2017
//    [2017] Art-directed watercolor stylization of 3D animations in real-time
fragmentOutput gapsOverlapsFrag(vertexOutputSampler i) {
    fragmentOutput result;
    int2 loc = int2(i.pos.xy);

    float eEdges = LOAD_TEXTURE2D(_EdgeTex, loc).b;
    float gapsOverlaps = LOAD_TEXTURE2D(_EdgeCtrlTex, loc).b * _GapsOverlapsWidth;  // edge control target fidelity (b)
    float blending = LOAD_TEXTURE2D(_BlendingTex, loc).a;  // contains the blending mask
    
    float4 outColor = LOAD_TEXTURE2D(_MainTex, loc);
    float mask = outColor.a;
    float goThreshold = 1.0 / _GapsOverlapsWidth;

    // make sure we are not considering emptiness or blending
    if (mask>0.1 && blending < 0.01) {
        // make sure we are at an edge
        if (eEdges > 0.1) {


            // OVERLAPS
            if (gapsOverlaps > 0.1f) {

                // get gradients
                float right = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, 0)).b;
                float left = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, 0)).b;
                float down = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(0, _Texel.y)).b;
                float up = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(0, -_Texel.y)).b;

                float topRight = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, -_Texel.y)).b;
                float topLeft = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, -_Texel.y)).b;
                float downRight = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, _Texel.y)).b;
                float downLeft = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, _Texel.y)).b;

                // could be optimized for lower end devices by using bilinear filtering
                float uGradient = (right + 0.5*(topRight + downRight)) - (left + 0.5 * (topLeft + downLeft));
                float vGradient = (down + 0.5*(downRight + downLeft)) - (up + 0.5*(topRight + topLeft));
                float2 gradient = float2(uGradient, vGradient);
                float4 destColor = outColor;

                gradient = normalize(gradient);

                int o = 1;
                // find vector of gradient (to get neighboring color)
                [unroll(10)] for (o = 1; o < _GapsOverlapsWidth; o++) {
                    if (gapsOverlaps < o) {
                        break;
                    }
                    destColor = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + o*(gradient*_Texel));
                    // check with destination color
                    if (length(destColor - outColor) > 0.33) {
                        // no overlap with substrateColor
                        if (length(destColor.rgb - _SubstrateColor) < 0.1) {
                            break;
                        }
                        outColor.rgb = mixRYB2(outColor.rgb, destColor.rgb);
                        break;
                    }
                }
                // check if loop reached the max
                if (o == _GapsOverlapsWidth) {
                    // means that gradient was reversed
                    //return float4(1.0, 0, 0, 1.0);
                    destColor = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + (-gradient*_Texel));
                    outColor.rgb = mixRYB2(outColor.rgb, destColor.rgb);
                }
            }


            // GAPS
            if (gapsOverlaps < -0.1f) {
                // check if it is at an edge
                if (eEdges > goThreshold*2) {
                    //result.colorOutput = float4(0, 0, 0, 0);
                    result.colorOutput = float4(_SubstrateColor, outColor.a);
                    result.alphaOutput = 0.0;
                    return result;
                }

                // get gradients
                float right = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, 0)).b;
                float left = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, 0)).b;
                float down = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(0, _Texel.y)).b;
                float up = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(0, -_Texel.y)).b;

                float topRight = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, -_Texel.y)).b;
                float topLeft = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, -_Texel.y)).b;
                float downRight = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(_Texel.x, _Texel.y)).b;
                float downLeft = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + float2(-_Texel.x, _Texel.y)).b;

                float uGradient = (right + 0.5*(topRight + downRight)) - (left + 0.5 * (topLeft + downLeft));
                float vGradient = (down + 0.5*(downRight + downLeft)) - (up + 0.5*(topRight + topLeft));
                float2 gradient = float2(uGradient, vGradient);

                // normalize gradient to check neighboring pixels
                gradient = normalize(gradient);
                [unroll(10)] for (int o = 1; o < _GapsOverlapsWidth; o++) {
                    if (abs(gapsOverlaps) < o / _GapsOverlapsWidth) {
                        //outColor.rgb = float3(gapsOverlaps, 0, 0);
                        break;
                    }
                    float destEdges = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, i.uv + o*(gradient*_Texel)).b;
                    // check destionation edge
                    if (destEdges > goThreshold) {
                        outColor.rgb = _SubstrateColor;
                        break;
                    }
                }
            }
        }
    }

    result.colorOutput = float4(outColor);
    result.alphaOutput = outColor.a;

    return result;
}