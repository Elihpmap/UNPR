////////////////////////////////////////////////////////////////////////////////////////////////////
// quadPigmentApplication10.fx (HLSL)
// Brief: Defining how pigments are applied
// Contributors: Santiago Montesdeoca, Amir Semmo
////////////////////////////////////////////////////////////////////////////////////////////////////
//          _                            _                         _ _           _   _
//    _ __ (_) __ _ _ __ ___   ___ _ __ | |_      __ _ _ __  _ __ | (_) ___ __ _| |_(_) ___  _ __
//   | '_ \| |/ _` | '_ ` _ \ / _ \ '_ \| __|    / _` | '_ \| '_ \| | |/ __/ _` | __| |/ _ \| '_ \
//   | |_) | | (_| | | | | | |  __/ | | | |_    | (_| | |_) | |_) | | | (_| (_| | |_| | (_) | | | |
//   | .__/|_|\__, |_| |_| |_|\___|_| |_|\__|    \__,_| .__/| .__/|_|_|\___\__,_|\__|_|\___/|_| |_|
//   |_|      |___/                                   |_|   |_|
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader file provides different algorithms for pigment application in different media
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"

// TEXTURES
Texture2D _FilterTex; // Only used for oil painting (not implemented in Unity yet)
Texture2D _SubstrateTargetTex;
Texture2D _PigmentCtrlTex;


// VARIABLES
float3 _SubstrateColor = float3(1.0, 1.0, 1.0);
float _PigmentDensity;
float _DryBrushThreshold;


// MRT 
struct fragmentOutput {
    float4 colorOutput : SV_Target0;
    float alphaOutput : SV_Target1;
};



//                 _                            _         _                  _                    _
//   __      _____| |_           __ _ _ __   __| |     __| |_ __ _   _      | |__  _ __ _   _ ___| |__
//   \ \ /\ / / _ \ __|____     / _` | '_ \ / _` |    / _` | '__| | | |_____| '_ \| '__| | | / __| '_ \
//    \ V  V /  __/ ||_____|   | (_| | | | | (_| |   | (_| | |  | |_| |_____| |_) | |  | |_| \__ \ | | |
//     \_/\_/ \___|\__|         \__,_|_| |_|\__,_|    \__,_|_|   \__, |     |_.__/|_|   \__,_|___/_| |_|
//                                                               |___/
// Contributor: Santiago Montesdeoca
// [WC] - Defines how watercolor is applied:
// - Wet, accumulating pigments at the valleys of the paper (aka substrate granulation)
// - Dry, showing pigments that have only been applied at the peaks of the paper
// -> Based on the pigment application model by Montesdeoca et al. 2017
//    [2017] Edge- and substrate-based effects for watercolor stylization
float4 pigmentApplicationWCFrag(vertexOutput i) : SV_Target {
    int2 loc = int2(i.pos.xy);  // coordinates for loading

    float4 renderTex = LOAD_TEXTURE2D(_MainTex, loc);
    float heightMap = LOAD_TEXTURE2D(_SubstrateTargetTex, loc).b;  // heightmap is embedded in the blue channel of the surfaceTex
    float application = LOAD_TEXTURE2D(_PigmentCtrlTex, loc).g;  // dry brush --- wet brush, pigments control target (r)
    float mask = renderTex.a;

    // check if its not the substrate color
    if (mask < 0.01) {
        return renderTex;
    }

    //calculate drybrush
    float dryDiff = heightMap + application;
    if (dryDiff < 0) {
        return lerp(renderTex, float4(_SubstrateColor, renderTex.a), saturate(abs(dryDiff)*_DryBrushThreshold));
    } else {
        // calculate density accumulation (1 granulate, 0 default)
        application = (abs(application) + 0.2);  // default is granulated (// 1.2 granulate, 0.2 default)

        //more accumulation on brighter areas
        application = lerp(application, application * 5, luminance(renderTex.rgb));  // deprecated
        //application = lerp(application, application * 4, luminance(renderTex.rgb));  // new approach

        //modulate heightmap to be between 0.8-1.0 (for montesdeoca et al. 2016)
        heightMap = (heightMap * 0.2) + 0.8;  // deprecated
    }

    //montesdeoca et al.
    float accumulation = 1 + (_PigmentDensity * application * (1 - (heightMap)));

    //calculate denser color output
    float3 colorOut = pow(abs(renderTex.rgb), accumulation);

    // don't granulate/dry-brush if the renderTex is similar to substrate color
    float colorDiff = saturate(length(renderTex.rgb - _SubstrateColor) * 5);
    colorOut = lerp(renderTex.rgb, colorOut, colorDiff.xxx);
    return float4(colorOut, renderTex.a);
}



//        _              _                    _
//     __| |_ __ _   _  | |__  _ __ _   _ ___| |__
//    / _` | '__| | | | | '_ \| '__| | | / __| '_ \
//   | (_| | |  | |_| | | |_) | |  | |_| \__ \ | | |
//    \__,_|_|   \__, | |_.__/|_|   \__,_|___/_| |_|
//               |___/
// [OP] - Defines how oil is applied:
// - Thick, accumulating pigments at the valleys of the paper
// - Dry, showing pigments that have only been applied at the peaks of the paper
// -> Based on the watercolor dry brush, from the pigment application model by Montesdeoca et al. 2017
//    [2017] Edge- and substrate-based effects for watercolor stylization
fragmentOutput pigmentApplicationOPFrag(vertexOutput i) {
    fragmentOutput result;
    int2 loc = int2(i.pos.xy);  // coordinates for loading

    float4 renderTex = LOAD_TEXTURE2D(_MainTex, loc);
    float filterTex = LOAD_TEXTURE2D(_FilterTex, loc).x;
    float heightMap = LOAD_TEXTURE2D(_SubstrateTargetTex, loc).b;  // heightmap is embedded in the blue channel of the surfaceTex
    float application = LOAD_TEXTURE2D(_PigmentCtrlTex, loc).g;  // dry brush --- wet brush, unpack pigments control target (x)
    float mask = renderTex.a;

    // check if its not the substrate
    if (mask < 0.01) {
        result.colorOutput = renderTex;
        result.alphaOutput = filterTex;
        return result;
    }

    //calculate drybrush
    float dryBrush = -application;
    float dryDiff = heightMap - dryBrush;
    if (dryDiff < 0) {
        float alpha = saturate(abs(dryDiff)*_DryBrushThreshold);
        result.colorOutput = lerp(renderTex, float4(_SubstrateColor, renderTex.a), alpha);
        result.alphaOutput = filterTex * (1.0 - alpha);
        return result;
    }
    else {
        // calculate density accumulation (-1 granulate, 0 default)
        dryBrush = (abs(dryBrush) + 0.2);  // default is granulated (// 1.2 granulate, 0.2 default)

        //more accumulation on brighter areas
        dryBrush = lerp(dryBrush, dryBrush * 5, luminance(renderTex.rgb));

        //modulate heightmap to be between 0.8-1.0 (for montesdeoca et al.)
        heightMap = (heightMap * 0.2) + 0.8;
    }

    //montesdeoca et al.
    float accumulation = 1 + (dryBrush * (1 - (heightMap)) * _PigmentDensity);

    //calculate denser color output
    float3 colorOut = pow(abs(renderTex.rgb), accumulation);

    // don't granulate/dry-brush if the renderTex is similar to substrate color
    float colorDiff = saturate(length(renderTex.rgb - _SubstrateColor) * 5);
    colorOut = lerp(renderTex.rgb, colorOut, colorDiff.xxx);

    result.colorOutput = float4(colorOut, renderTex.a);
    result.alphaOutput = filterTex;
    return result;
}