////////////////////////////////////////////////////////////////////////////////////////////////////
// quadSeparable10.fx (HLSL)
// Brief: Separable filters for watercolor stylization
// Contributors: Santiago Montesdeoca
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                   _     _                        _                     _            
//    ___  ___ _ __   __ _ _ __ __ _| |__ | | ___    __      ____ _| |_ ___ _ __ ___ ___ | | ___  _ __ 
//   / __|/ _ \ '_ \ / _` | '__/ _` | '_ \| |/ _ \   \ \ /\ / / _` | __/ _ \ '__/ __/ _ \| |/ _ \| '__|
//   \__ \  __/ |_) | (_| | | | (_| | |_) | |  __/    \ V  V / (_| | ||  __/ | | (_| (_) | | (_) | |   
//   |___/\___| .__/ \__,_|_|  \__,_|_.__/|_|\___|     \_/\_/ \__,_|\__\___|_|  \___\___/|_|\___/|_|   
//            |_|                                                                                      
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader file provides separable filters to achieve the following:
// - Bleeding blur that will be blended later on to generate color bleeding
// - Extend the edges to converge edges into gaps and overlaps
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"

// TEXTURES
TEXTURE2D_SAMPLER2D (_EdgeTex, sampler_EdgeTex);
TEXTURE2D_SAMPLER2D (_LinearDepthTex, sampler_LinearDepthTex);
TEXTURE2D_SAMPLER2D (_EdgeCtrlTex, sampler_EdgeCtrlTex);
TEXTURE2D_SAMPLER2D (_AbstractCtrlTex, sampler_AbstractCtrlTex);


// VARIABLES
float _RenderScale = 1.0; // TODO really necessary?
float _BleedingThreshold = 0.0002;
float _EdgeDarkeningWidth = 3;
float _GapsOverlapsWidth = 3;
float _BleedingRadius = 20;
float _BleedingWeights[161];  // max 40 bleeding radius (supersampled would be 80) // TODO supersampled?


// MRT output struct
struct fragmentOutput {
    float4 bleedingBlur : SV_Target0;
    float4 darkenedEdgeBlur : SV_Target1;
};



//     __                  _   _                 
//    / _|_   _ _ __   ___| |_(_) ___  _ __  ___ 
//   | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
//   |  _| |_| | | | | (__| |_| | (_) | | | \__ \
//   |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
//
// SIGMOID WEIGHT
float sigmoidWeight(float x) {
    float weight = 1.0 - x;  // inverse normalized gradient | 0...0,5...1...0,5...0

    weight = weight * 2.0 - 1.0;  // increase amplitude by 2 and shift by -1 | -1...0...1...0...-1 (so that 0,5 in the gradient is not 0
    weight = (-weight * abs(weight) * 0.5) + weight + 0.5;  // square the weights(fractions) and multiply them by 0.5 to accentuate sigmoid
    //return dot(float3(-weight, 2.0, 1.0 ),float3(abs(weight), weight, 1.0)) * 0.5;  // possibly faster version?

    return weight;
}


// COSINE WEIGHT
float cosineWeight(float x) {
    float weight = cos(x * PI / 2);
    return weight;
}


// GAUSSIAN WEIGHT
float gaussianWeight(float x, float sigma) {
    float weight = 0.15915*exp(-0.5*x*x / (sigma*sigma)) / sigma;
    //float weight = pow((6.283185*sigma*sigma), -0.5) * exp((-0.5*x*x) / (sigma*sigma));
    return weight;
}


// LINEAR WEIGHT
float linearWeight(float x) {
    float weight = 1.0 - x;
    return weight;
}



//             _                _     _            
//     ___  __| | __ _  ___    | |__ | |_   _ _ __ 
//    / _ \/ _` |/ _` |/ _ \   | '_ \| | | | | '__|
//   |  __/ (_| | (_| |  __/   | |_) | | |_| | |   
//    \___|\__,_|\__, |\___|   |_.__/|_|\__,_|_|   
//               |___/                             

// Contributors: Santiago Montesdeoca
// Extends the edges for darkened edges and gaps and overlaps
float3 edgeBlur(float2 uv, float2 dir) {

    // sample center pixel
    float3 sEdge = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, uv).rgb;

    // calculate darkening width
    float edgeWidthCtrl = SAMPLE_TEXTURE2D(_EdgeCtrlTex, sampler_EdgeCtrlTex, uv).g;  // edge control target (g)

    float paintedWidth = lerp(0, _EdgeDarkeningWidth * 3, edgeWidthCtrl);  // 3 times wider through local control
    int kernelRadius = max(1, (_EdgeDarkeningWidth + paintedWidth));  // global + local control
    float normalizer = 1.0 / kernelRadius;

    /// experimental weights
    //sigmoid blur
    //float weight = sigmoidWeight(0.0);
    //cosine blur
    //float weight = cosineWeight(0.0);
    //gaussian blur
    float sigma = kernelRadius / 2.0;
    float weight = gaussianWeight(0.0, sigma);

    float darkEdgeGradient = sEdge.g * weight;
    float normDivisor = weight;

    //EDGE DARKENING GRADIENT
    //continue with lateral pixels (unroll is used to fix the dynamic loop at a certain amount)
    [unroll(100)] for (int o = 1; o < kernelRadius; o++) {
    //for (int o = 1; o < _EdgeDarkeningWidth; o++) {
        float offsetColorR = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, saturate(uv + float2(o*dir))).g;
        float offsetColorL = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, saturate(uv + float2(-o*dir))).g;

        // using a sigmoid weight
        //float normGradient = (abs(o) * normalizer); //normalized gradient | 1...0,5...0...0,5...1
        //weight = sigmoidWeight(normGradient);
        // using a sinusoidal weight
        //weight = cosineWeight(normGradient);
        // using a gaussian weight
        weight = gaussianWeight(o, sigma);

        darkEdgeGradient += weight * (offsetColorL + offsetColorR);
        normDivisor += weight * 2;
    }
    darkEdgeGradient = (darkEdgeGradient / normDivisor);


    //GAPS AND OVERLAPS GRADIENT
    weight = linearWeight(0.0);
    float linearGradient = sEdge.b * weight;
    normDivisor = weight;
    normalizer = 1.0 / _GapsOverlapsWidth;

    for (int l = 1; l < _GapsOverlapsWidth; l++) {
        float offsetColorR = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, saturate(uv + float2(l*dir))).b;
        float offsetColorL = SAMPLE_TEXTURE2D(_EdgeTex, sampler_EdgeTex, saturate(uv + float2(-l*dir))).b;
        float normGradient = (l * normalizer); //normalized gradient | 1...0,5...0...0,5...1

        weight = linearWeight(normGradient);

        linearGradient += weight * (offsetColorL + offsetColorR);
        normDivisor += weight * 2;
    }

    linearGradient = linearGradient / normDivisor;

    return float3(sEdge.r, darkEdgeGradient, linearGradient);
}



//              _                _     _               _ _             
//     ___ ___ | | ___  _ __    | |__ | | ___  ___  __| (_)_ __   __ _ 
//    / __/ _ \| |/ _ \| '__|   | '_ \| |/ _ \/ _ \/ _` | | '_ \ / _` |
//   | (_| (_) | | (_) | |      | |_) | |  __/  __/ (_| | | | | | (_| |
//    \___\___/|_|\___/|_|      |_.__/|_|\___|\___|\__,_|_|_| |_|\__, |
//                                                               |___/ 

// Contributors: Santiago Montesdeoca
// Blurs certain parts of the image for color bleeding
float4 colorBleeding(float2 uv, float2 dir) {
    float4 blurPixel = float4(0.0, 0.0, 0.0, 0.0);
    
    // get source pixel values
    float sDepth = SAMPLE_TEXTURE2D(_LinearDepthTex, sampler_LinearDepthTex, uv).r;
    float sBlurCtrl = 0;
    if (dir.y > 0) {
        sBlurCtrl = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv).a;
    } else {
        sBlurCtrl = SAMPLE_TEXTURE2D(_AbstractCtrlTex, sampler_AbstractCtrlTex, uv).b;  // abstraction control target (b)
    }
    float4 sColor = float4(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv).rgb, sBlurCtrl);

    // go through neighborhood
    for (int a = -_BleedingRadius; a <= _BleedingRadius; a++) {
        float2 offsetUV = saturate(uv + float2(a*dir));

        // get destination values
        float dBlurCtrl = 0;
        if (dir.y > 0) {
            dBlurCtrl = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, offsetUV).a;
        } else {
            dBlurCtrl = SAMPLE_TEXTURE2D(_AbstractCtrlTex, sampler_AbstractCtrlTex, offsetUV).b;  // abstraction control target (b)
        }
        float dDepth = SAMPLE_TEXTURE2D(_LinearDepthTex, sampler_LinearDepthTex, offsetUV).r;


        // BILATERAL DEPTH BASED BLEEDING
        float weight = _BleedingWeights[a + _BleedingRadius];
        
        float ctrlScope = max(dBlurCtrl, sBlurCtrl);
        float filterScope = abs(a) / _BleedingRadius;
        // check if source or destination pixels are bleeded
        if ((dBlurCtrl > 0) || (sBlurCtrl > 0)) {
        //if (ctrlScope >= filterScope) {

            float bleedQ = 0;
            bool sourceBehindQ = false;
            // check if source pixel is behind
            if ((sDepth - _BleedingThreshold) > dDepth) {
                sourceBehindQ = true;
            }

            // check bleeding cases
            if ((dBlurCtrl) && (sourceBehindQ)) {
                bleedQ = 1;
            } else {
                if ((sBlurCtrl) && (!sourceBehindQ)) {
                    bleedQ = 1;
                }
            }

            // bleed if necessary
            if (bleedQ) {
                float4 dColor = float4(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, offsetUV).rgb, dBlurCtrl);
                blurPixel += dColor * weight;  // bleed
            } else {
                blurPixel += sColor * weight;  // get source pixel color
            }
        } else {
            blurPixel += sColor * weight;
            // blurPixel += float4(0.1, 0.0, 0.0, 0.0);
        }
    }

    return blurPixel;
}



//    _                _                _        _ 
//   | |__   ___  _ __(_)_______  _ __ | |_ __ _| |
//   | '_ \ / _ \| '__| |_  / _ \| '_ \| __/ _` | |
//   | | | | (_) | |  | |/ / (_) | | | | || (_| | |
//   |_| |_|\___/|_|  |_/___\___/|_| |_|\__\__,_|_|
//                                                 
fragmentOutput horizontalFrag(vertexOutputSampler i) {
    fragmentOutput result;

    float2 offset = _Texel * float2(1.0f, 0.0f);

    // run different blurring algorithms
    result.bleedingBlur = colorBleeding(i.uv, offset);
    result.darkenedEdgeBlur = float4(edgeBlur(i.uv, offset), 0);

    //result.bleedingBlur = float4(1.0, 0, 0, 1.0);
    return result;
}



//                   _   _           _ 
//   __   _____ _ __| |_(_) ___ __ _| |
//   \ \ / / _ \ '__| __| |/ __/ _` | |
//    \ V /  __/ |  | |_| | (_| (_| | |
//     \_/ \___|_|   \__|_|\___\__,_|_|
//                                     
fragmentOutput verticalFrag(vertexOutputSampler i) {
    fragmentOutput result;

    float2 offset = _Texel * float2(0.0f, 1.0f);

    // run different blurring algorithms
    result.bleedingBlur = colorBleeding(i.uv, offset);
    result.darkenedEdgeBlur = float4(edgeBlur(i.uv, offset), result.bleedingBlur.a);
    result.darkenedEdgeBlur.b = pow(result.darkenedEdgeBlur.b, 1 / result.darkenedEdgeBlur.b);  // get rid of weak gradients
    result.darkenedEdgeBlur.b = pow(result.darkenedEdgeBlur.b, 2 / _GapsOverlapsWidth);  // adjust gamma depending on kernel size

    return result;
}
