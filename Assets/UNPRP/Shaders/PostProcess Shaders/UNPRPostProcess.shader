﻿Shader "Hidden/UNPR/PostProcess"
{
    HLSLINCLUDE
        //TODO clean this content...
        #include "../../ShaderLibrary/Common.hlsl"

        vertexOutputSampler clipVertex(
            float4 vertex : POSITION, // vertex position input
            float2 uv : TEXCOORD0 // first texture coordinate input
        )
        {
            vertexOutputSampler o;
            /*
            float3 positionWS = TransformObjectToWorld(vertex);
            o.positionCS = TransformWorldToHClip(positionWS);
            */
            o.pos = vertex;
            o.uv = uv;
            return o;
        }


    ENDHLSL

    Properties
    {
        //_MainTex("Texture", any) = "" {} // stolen from Hidden/BlitCopy the "any" type seems to be meaning any texture... but I couldn't find any documentation to back this
        //TODO add textures names?
    }

    SubShader
    {
        //https://docs.unity3d.com/Manual/shader-shaderlab-commands.html
        Cull Off 
        ZWrite Off 
        ZTest Always
    
        Pass
        {
            Name "AdjustLoad"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment adjustLoadFrag 
                #include "AdjustLoad.hlsl"

            ENDHLSL
        }

        Pass
        {
            Name "EdgeDetection"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment sobelRGBDFrag 
                #include "EdgeDetection.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Pigment Density"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment pigmentDensityWCFrag 
                #include "PigmentManipulation.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Separable H"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment horizontalFrag 
                #include "Separable.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Separable V"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment verticalFrag 
                #include "Separable.hlsl" 

            ENDHLSL
        }
        Pass
        {
            Name "Bleeding"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment blendCtrlAlphaFrag
                #include "Blend.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Edge Darkenning"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment gradientEdgesWCFrag
                #include "EdgeManipulation.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Gaps And Overlaps"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment gapsOverlapsFrag 
                #include "GapsOverlaps.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Pigment Application"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment pigmentApplicationWCFrag 
                #include "PigmentApplication.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Substrate Distorsion"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment substrateDistortionFrag 
                #include "Substrate.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "AntiAliasing"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment FXAAFrag
                #include "AA.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Substrate Lightning"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment deferredLightingFrag 
                #include "Substrate.hlsl"

            ENDHLSL
        }
    }
}
