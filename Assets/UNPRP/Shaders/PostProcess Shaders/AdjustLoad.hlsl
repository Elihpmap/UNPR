////////////////////////////////////////////////////////////////////////////////////////////////////
// AdjustLoad.hlsl (HLSL) A Convertir TODO
// Brief: Adjusting and loading render targets
// Contributors: Santiago Montesdeoca, Yee Xin Chiew, Amir Semmo
////////////////////////////////////////////////////////////////////////////////////////////////////
//              _  _           _        _                 _
//     __ _  __| |(_)_   _ ___| |_     | | ___   __ _  __| |
//    / _` |/ _` || | | | / __| __|____| |/ _ \ / _` |/ _` |
//   | (_| | (_| || | |_| \__ \ ||_____| | (_) | (_| | (_| |
//    \__,_|\__,_|/ |\__,_|___/\__|    |_|\___/ \__,_|\__,_|
//              |__/
////////////////////////////////////////////////////////////////////////////////////////////////////
// This base shader adjusts and loads any required elements for future stylization in MNPR
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"

// RENDER TEXTURES
Texture2D _DiffuseTex;          // diffuse
Texture2D _SpecularTex;         // specular
Texture2D _DepthTex;            // ZBuffer
Texture2D _LinearDepthTex;      // linearized depth
Texture2D _VelocityTex;         // velocity


// VARIABLES
// Texture
TEXTURE2D_SAMPLER2D (_SubstrateTex, sampler_SubstrateTex);      // substrate texture (paper, canvas, etc)
float4 _SubstrateTex_ST; // x= X tilling, y= Y tilling, z= X offset, w= Y offset
float4 _SubstrateTex_TexelSize; // x= 1/width, y= 1/height, z= width, w= height

float2 _SubstrateOffset;

// post-processing effects
float _Saturation = 1.0;
float _Contrast = 1.0;
float _Brightness = 1.0;

// engine settings
float _Gamma = 1.0;
float _NearClipPlane;
float2 _DepthRange = float2(8.0, 50.0);
float3 _SubstrateColor = float3(1.0, 1.0, 1.0);
float _EnableVelocityPV;
float _SubstrateRoughness;
float _SubstrateScale;
float3 _AtmosphereTint;
float2 _AtmosphereRange;

// MRT
struct fragmentOutput 
{
	float4 stylizationOutput : SV_Target0;
	float4 substrateOutput : SV_Target1;
	float2 depthOutput : SV_Target2;
    float2 velocity : SV_Target3;
};

float4 _ZBufferParams;

//     __                  _   _
//    / _|_   _ _ __   ___| |_(_) ___  _ __  ___
//   | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
//   |  _| |_| | | | | (__| |_| | (_) | | | \__ \
//   |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
//
// remap range
float remap(float value, float oldMin, float oldMax, float newMin, float newMax) {
	return newMin + (((value - oldMin) / (oldMax - oldMin)) * (newMax - newMin));
}



//              _  _           _             _                 _
//     __ _  __| |(_)_   _ ___| |_          | | ___   __ _  __| |
//    / _` |/ _` || | | | / __| __|  _____  | |/ _ \ / _` |/ _` |
//   | (_| | (_| || | |_| \__ \ |_  |_____| | | (_) | (_| | (_| |
//    \__,_|\__,_|/ |\__,_|___/\__|         |_|\___/ \__,_|\__,_|
//              |__/

// Contributors: Santiago Montesdeoca, Yee Xin Chiew, Amir Semmo
// This shader performs four operations:
// 1.- Simple color post processing operations over the Maya render (tgt 1)
// 2.- Adds the substrate color as the background color (tgt 1)
// 3.- Loads the substrate texture into the substrate target (tgt 2)
// 4.- Modulates Maya's Z-buffer to a linear depth target with a custom range (tgt 3)
// These four operations are condensed in one pass/fragment in the original MNPR and were separated to fit Unity
fragmentOutput adjustLoadFrag(vertexOutputSampler i)
{
    fragmentOutput result;
    int2 loc = int2(i.pos.xy);  // coordinates for loading texels

    // LINEAR DEPTH

    float zBuffer = LOAD_TEXTURE2D(_DepthTex, (loc)).r;
    float depth;
    float distanceFromCamera = LinearEyeDepth(zBuffer, _ZBufferParams);
    if (distanceFromCamera < _DepthRange.x)
    {
        depth = 0;
    }
    else if (distanceFromCamera > _DepthRange.y)
    {
        depth = 1;
    }
    else
    { 
        depth = remap(distanceFromCamera, _DepthRange.x, _DepthRange.y, 0, 1);
    }
    // save depth of previous frame
    float depthPrevious = LOAD_TEXTURE2D(_LinearDepthTex, (loc)).r;
    result.depthOutput = float2(depth, depthPrevious);

    
	// POST-PROCESSING // TODO part to review with custom diffuse and specular targets
	// get pixel value
	float4 renderTex = LOAD_TEXTURE2D(_MainTex, (loc));
    float4 diffuseTex = LOAD_TEXTURE2D(_DiffuseTex, (loc));
    //float depthOfStylizedShaders = LOAD_TEXTURE2D(_SpecularTex, _MainTex_TexelSize, (loc)).a;
    float mask = LOAD_TEXTURE2D(_SpecularTex, (loc)).a;
    if (mask > 0) {
        if (_Gamma < 1) {
            // if viewport is not gamma corrected, at least keep light linearity
            diffuseTex.rgb = pow(abs(diffuseTex.rgb), 0.454545455); // TODO need to use abs()? (isn't present in the original MNPR)
        }
        //renderTex.rgb *= lightingTex.rgb;
        // shade color embedded in negative values of lightingTex
        //float3 shadeColor = lerp(saturate(lightingTex * float3(-1, -1, -1)))
        renderTex.rgb *= diffuseTex.rgb;
        renderTex.rgb += LOAD_TEXTURE2D(_SpecularTex, (loc)).rgb;  // add specular contribution
    }
	// color operations
	float luma = luminance(renderTex.rgb);
	float3 saturationResult = float3(lerp(luma.xxx, renderTex.rgb, _Saturation));
	float3 contrastResult = lerp(float3(0.5, 0.5, 0.5), saturationResult, _Contrast * 0.5 + 0.5);
	float b = _Brightness - 1.0;
	float3 brightnessResult = saturate(contrastResult.rgb + b);

    // atmospheric operations
    float remapedDepth = saturate(remap(distanceFromCamera, _AtmosphereRange.x, _AtmosphereRange.y, 0, 1));
    float3 atmosphericResult = lerp(brightnessResult, _AtmosphereTint, remapedDepth);

	// add substrate color
	renderTex.rgb = lerp(_SubstrateColor, atmosphericResult, renderTex.a);
	result.stylizationOutput = float4(renderTex.rgb,1);
    
    // SUBSTRATE
    float2 SubstrateTexDimensions = _SubstrateTex_TexelSize.zw;
    //float2 SubstrateTexOffset = _SubstrateTex_ST.zw;
    float2 SubstrateTexOffset = _SubstrateOffset;

	// get proper UVS
	float2 uv = i.uv * (_ScreenSize / SubstrateTexDimensions) * (_SubstrateScale) + SubstrateTexOffset;
	// get substrate pixel
	float3 substrate = SAMPLE_TEXTURE2D(_SubstrateTex, sampler_SubstrateTex, uv).rgb;
	substrate = substrate - 0.5;  // bring to [-0.5 - 0 - 0.5]
	substrate *= _SubstrateRoughness;  // define roughness
    result.substrateOutput = float4(substrate + 0.5, 0.0);  // bring back to [0 - 1]

    // VELOCITY (reset if disabled)
    result.velocity = (_EnableVelocityPV == 1.0 ? LOAD_TEXTURE2D(_VelocityTex, (loc)).xy : float2(0.0, 0.0));

    return result;
}