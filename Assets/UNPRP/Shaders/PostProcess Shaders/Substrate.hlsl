////////////////////////////////////////////////////////////////////////////////////////////////////
// quadSubstrate10.fx (HLSL)
// Brief: Substrate operations for MNPR
// Contributors: Santiago Montesdeoca, Amir Semmo
////////////////////////////////////////////////////////////////////////////////////////////////////
//              _         _             _       
//    ___ _   _| |__  ___| |_ _ __ __ _| |_ ___ 
//   / __| | | | '_ \/ __| __| '__/ _` | __/ _ \
//   \__ \ |_| | |_) \__ \ |_| | | (_| | ||  __/
//   |___/\__,_|_.__/|___/\__|_|  \__,_|\__\___|
//
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader file provides algorithms for substrate-based effects in MNPR
// 1.- Substrate lighting, adding an external lighting source to the render
// 2.- Substrate distortion, emulating the substrate fingering happening on rough substrates
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"


// TEXTURES
TEXTURE2D_SAMPLER2D (_SubstrateTargetTex, sampler_SubstrateTargetTex);
Texture2D _EdgeTex;
Texture2D _SubstrateCtrlTex;
TEXTURE2D_SAMPLER2D (_LinearDepthTex, sampler_LinearDepthTex);


// VARIABLES
float _Gamma = 1.0;
float _SubstrateLightDir = 0;
float _SubstrateLightTilt = 45;
float _SubstrateShading = 1.0;
float _SubstrateDistortion;

float _ImpastoPhongSpecular = 0.6;
float _ImpastoPhongShininess = 16.0;


// BLENDING
float blendOverlay(in float base, in float blend) {
    return base < 0.5 ? (2.0*base*blend) : (1.0 - 2.0*(1.0 - base)*(1.0 - blend));
}

float blendLinearDodge(in float base, in float blend) {
    return base + blend;
}



//    _ _       _     _   _             
//   | (_) __ _| |__ | |_(_)_ __   __ _ 
//   | | |/ _` | '_ \| __| | '_ \ / _` |
//   | | | (_| | | | | |_| | | | | (_| |
//   |_|_|\__, |_| |_|\__|_|_| |_|\__, |
//        |___/                   |___/ 

// Contributor: Santiago Montesdeoca
// Calculates the substrate lighting on top of the rendered imagery
// -> Based on the external substrate lighting model by Montesdeoca et al. 2017
//    [2017] Edge- and substrate-based effects for watercolor stylization
float4 deferredLightingFrag(vertexOutputSampler i) : SV_Target {
	int2 loc = int2(i.pos.xy);
	float4 renderTex = LOAD_TEXTURE2D(_MainTex, loc);
	float2 substrateNormalTex = SAMPLE_TEXTURE2D(_SubstrateTargetTex, sampler_SubstrateTargetTex, i.uv).rg - 0.5;  // bring normals to [-0.5 - 0.5]

	// get light direction
	float dirRadians = _SubstrateLightDir * PI / 180.0;
	float3 lightDir = float3(sin(dirRadians), cos(dirRadians), (_SubstrateLightTilt / 89.0));

	// calculate diffuse illumination
	float3 normals = float3(-substrateNormalTex, 1.0);
	float diffuse = dot(normals, lightDir);  // basic lambert
	//diffuse = 1.0 - acos(diffuse)/PI;  // angular lambert
	//diffuse = (1 + diffuse)*0.5;  // extended lambert

	// modulate diffuse shading
	diffuse = pow(1 - diffuse, 2);  // modify curve 
	diffuse = 1 - (diffuse * _SubstrateShading);

    // gamma correction on output
    if (_Gamma < 1) {
        // if viewport is not gamma corrected, at least keep light linearity on substrate
        diffuse = pow(diffuse, 0.454545455);
    }
    renderTex.rgb *= diffuse;
	return renderTex;
}

// Contributor: Amir Semmo
// Calculates the substrate lighting only on parts that have no paint applied (impasto would override any substrate structure)
// -> Extended from the external substrate lighting model by Montesdeoca et al. 2017
//    [2017] Edge- and substrate-based effects for watercolor stylization
float4 deferredImpastoLightingFrag(vertexOutputSampler i) : SV_Target {
    int2 loc = int2(i.pos.xy);
    float4 renderTex = LOAD_TEXTURE2D(_MainTex, loc);
    float3 substrateNormalTex = float3(clamp(SAMPLE_TEXTURE2D(_SubstrateTargetTex, sampler_SubstrateTargetTex, i.uv).rg - 0.5, -0.5, 0.5), 1.0); // bring normals to [-0.5 - 0.5]

    // get light direction
    float dirRadians = _SubstrateLightDir * PI / 180.0;
    float3 lightDir = float3(sin(dirRadians), cos(dirRadians), (_SubstrateLightTilt / 89.0));

    // calculate diffuse illumination
    float3 normals = float3(-substrateNormalTex.xy, 1.0);
    float diffuse = dot(normals, lightDir);  // basic lambert
    //diffuse = 1.0 - acos(diffuse)/PI;  // angular lambert
    //diffuse = (1 + diffuse)*0.5;  // extended lambert
    float2 phong = saturate(float2(diffuse, pow(diffuse, _ImpastoPhongShininess) * _ImpastoPhongSpecular));  // phong based

    // modulate diffuse shading
    diffuse = pow(1 - diffuse, 2);  // modify curve 
    diffuse = 1 - (diffuse * _SubstrateShading);

    // gamma correction on output
    if (_Gamma < 1) {
        // if viewport is not gamma corrected, at least keep light linearity on substrate
        diffuse = pow(diffuse, 0.454545455);
    }
    
    float3 substrateColor = lerp(renderTex.rgb*diffuse, renderTex.rgb, renderTex.a);
    float3 impastoColor = float3(blendOverlay(renderTex.r, phong.x), blendOverlay(renderTex.g, phong.x), blendOverlay(renderTex.b, phong.x)); // blend diffuse component
           impastoColor = float3(blendLinearDodge(phong.y, impastoColor.r), blendLinearDodge(phong.y, impastoColor.g), blendLinearDodge(phong.y, impastoColor.b));  // blend specular component

    // linearly blend with the alpha mask
    renderTex.rgb = lerp(substrateColor, impastoColor, renderTex.a);

    return renderTex;
}



//        _ _     _             _   _             
//     __| (_)___| |_ ___  _ __| |_(_) ___  _ __  
//    / _` | / __| __/ _ \| '__| __| |/ _ \| '_ \ 
//   | (_| | \__ \ || (_) | |  | |_| | (_) | | | |
//    \__,_|_|___/\__\___/|_|   \__|_|\___/|_| |_|
//                                                

// Contributor: Santiago Montesdeoca
// Calculates the substrate distortion generated by its roughness
// -> Based on the paper distortion approach by Montesdeoca et al. 2017
//    [2017] Art-directed watercolor stylization of 3D animations in real-time
float4 substrateDistortionFrag(vertexOutputSampler i) : SV_Target{
    int2 loc = int2(i.pos.xy); // coordinates for loading

    // get pixel values
    float2 normalTex = (LOAD_TEXTURE2D(_SubstrateTargetTex, loc).rg * 2 - 1);  // to transform float values to -1...1
    float distortCtrl = saturate(LOAD_TEXTURE2D(_SubstrateCtrlTex, loc).r + 0.2);  // control parameters, unpack substrate control target (y)
    float linearDepth = LOAD_TEXTURE2D(_LinearDepthTex, loc).r;

    // calculate uv offset
    float controlledDistortion = lerp(0, _SubstrateDistortion, 5.0*distortCtrl);  // 0.2 is default
    float2 uvOffset = normalTex * (controlledDistortion * _Texel);

    // check if destination is in front
    float depthDest = SAMPLE_TEXTURE2D(_LinearDepthTex, sampler_LinearDepthTex, i.uv + uvOffset).r;
    if (linearDepth - depthDest > 0.01) {
        uvOffset = float2(0.0f, 0.0f);
    }

    float4 colorDest = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + uvOffset);
    return colorDest;
}

// Contributor: Amir Semmo
// Calculates the substrate distortion generated by its roughness only close to edges
// -> Extended from the paper distortion approach by Montesdeoca et al. 2017
//    [2017] Art-directed watercolor stylization of 3D animations in real-time
float4 substrateDistortionEdgesFrag(vertexOutputSampler i) : SV_Target{
	int2 loc = int2(i.pos.xy); // coordinates for loading

	// get pixel values
	float2 normalTex = (LOAD_TEXTURE2D(_SubstrateTargetTex, loc).rg * 2 - 1);  // to transform float values to -1...1
	float distortCtrl = saturate(LOAD_TEXTURE2D(_SubstrateCtrlTex, loc).r + 0.2);  // control parameters, substrate control target (r)

	// calculate uv offset
	float controlledDistortion = lerp(0, _SubstrateDistortion, 5.0*distortCtrl);  // 0.2 is default
	float2 uvOffset = normalTex * (controlledDistortion * _Texel);
	float4 colorDest = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv + uvOffset);

    // only distort at edges
    float e = LOAD_TEXTURE2D(_EdgeTex, (int2(i.pos.xy))).x;

    return lerp(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv), colorDest, e);
}