﻿Shader "Hidden/UNPR/PostProcessUtilities"
{
    HLSLINCLUDE
        //TODO clean this content...
        #include "../../ShaderLibrary/Common.hlsl"

        vertexOutputSampler clipVertex(
            float4 vertex : POSITION, // vertex position input
            float2 uv : TEXCOORD0 // first texture coordinate input
        )
        {
            vertexOutputSampler o;
            /*
            float3 positionWS = TransformObjectToWorld(vertex);
            o.positionCS = TransformWorldToHClip(positionWS);
            */
            o.pos = vertex;
            o.uv = uv;
            return o;
        }

        float4 copyFrag(vertexOutputSampler i) : SV_Target
        {
            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv);
            //color = float4(1, 1, 0, 1);
            //color = float4(1, 1, 0, 1);
            return color;
        }

        TEXTURE2D(_DepthTex);

        float copyDepthFrag(vertexOutputSampler i) : SV_Depth
        {
            float depth = SAMPLE_DEPTH_TEXTURE(_MainTex, sampler_MainTex, i.uv);
            //float depth = LOAD_TEXTURE2D(_DepthTex, int2(i.pos.xy)).r;
            return depth; 
            // https://forum.unity.com/threads/how-do-i-copy-depth-to-the-camera-target-in-unitys-srp.713024/ ?
        }

        /*
        float4 copyVelocityFrag(vertexOutputSampler i) : SV_Target
        {
            float4 color = SAMPLE_TEXTURE2D(_CameraMotionVectorsTexture, sampler_CameraMotionVectorsTexture, i.uv); // TODO : Doesn't seem to work (not very important as velocity is not (yet) used in NPR)
            return color;
        }
        */

    ENDHLSL

    Properties
    {
        //_MainTex("Texture", any) = "" {} // stolen from Hidden/BlitCopy the "any" type seems to be meaning any texture... but I couldn't find any documentation to back this
    }

    SubShader
    {
        //Pass{}//TODO revise this... 

        Pass
        {
            Cull Off
            ZWrite Off
            ZTest Always
            Name "_MainTexCopy"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment copyFrag

            ENDHLSL
        }
    
        Pass
        {
            Cull Off
            ZWrite On
            ZTest Always
            Name "_CameraDepthTextureCopy"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment copyDepthFrag

            ENDHLSL
        }
        /*
        Pass
        {
            Cull Off
            ZWrite Off
            ZTest Always
            Name "_VelocityCopy"
            HLSLPROGRAM

                #pragma vertex clipVertex
                #pragma fragment copyVelocityFrag

            ENDHLSL
        }*/
    }
}
