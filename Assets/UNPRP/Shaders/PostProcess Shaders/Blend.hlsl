////////////////////////////////////////////////////////////////////////////////////////////////////
// quadBlend10.fx (HLSL)
// Brief: Blending algorithms
// Contributors: Santiago Montesdeoca
////////////////////////////////////////////////////////////////////////////////////////////////////
//    _     _                _ _             
//   | |__ | | ___ _ __   __| (_)_ __   __ _ 
//   | '_ \| |/ _ \ '_ \ / _` | | '_ \ / _` |
//   | |_) | |  __/ | | | (_| | | | | | (_| |
//   |_.__/|_|\___|_| |_|\__,_|_|_| |_|\__, |
//                                     |___/ 
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader provides alorithms for blending images such as:
// 1.- Blending from alpha of the blend texture
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
//#include "Common.hlsl"

// TEXTURES
Texture2D _BleedingTex;



//    _     _                _      __                               _       _           
//   | |__ | | ___ _ __   __| |    / _|_ __ ___  _ __ ___       __ _| |_ __ | |__   __ _ 
//   | '_ \| |/ _ \ '_ \ / _` |   | |_| '__/ _ \| '_ ` _ \     / _` | | '_ \| '_ \ / _` |
//   | |_) | |  __/ | | | (_| |   |  _| | | (_) | | | | | |   | (_| | | |_) | | | | (_| |
//   |_.__/|_|\___|_| |_|\__,_|   |_| |_|  \___/|_| |_| |_|    \__,_|_| .__/|_| |_|\__,_|
//                                                                    |_|                

// Contributor: Santiago Montesdeoca
// Blend image from alpha channel of blend texture
float4 blendCtrlAlphaFrag(vertexOutput i) : SV_Target {
	int2 loc = int2(i.pos.xy);

	// get pixel values
	float4 renderTex = LOAD_TEXTURE2D(_MainTex, loc);
	float4 blendTex = LOAD_TEXTURE2D(_BleedingTex, loc);
	float blendCtrl = saturate(blendTex.a*5.0);

	// blend colors
	float3 blendColor = lerp(renderTex.rgb, blendTex.rgb, blendCtrl.xxx);

	return float4(blendColor, renderTex.a+saturate(blendTex.a));
}