#ifndef UNPR_OPAQUE_PASS_INCLUDED
#define UNPR_OPAQUE_PASS_INCLUDED

#include "../ShaderLibrary/Common.hlsl"

//Properties
CBUFFER_START(UnityPerMaterial)
float4 _BaseColor;
//TEXTURE2D(_MainTex);
//SAMPLER(sampler_MainTex);
CBUFFER_END

struct VertexIn
{
    float3 objectPosition : POSITION;
    float4 color : COLOR;
    float2 baseUVs : TEXCOORD0;

    float2 pigmentRG : TEXCOORD2; 
    float2 substrateRG : TEXCOORD3;
    float2 edgeRG : TEXCOORD4;
    float2 abstractRG : TEXCOORD5;
    float2 pigmBsubsB : TEXCOORD6;
    float2 edgeBabstB : TEXCOORD7;
};

struct VertexOut
{
    float4 clipPosition : SV_POSITION;
    float4 color : COLOR0;
    float2 baseUVs : TEXCOORD0;

    float3 pigmentCtrlColor : COLOR1;
    float3 substrateCtrlColor : COLOR2;
    float3 edgeCtrlColor : COLOR3;
    float3 abstractCtrlColor : COLOR4;
};

//Vertex
VertexOut UNPROpaqueVertex(VertexIn vIn)
{
    VertexOut result;

    float3 positionWS = TransformObjectToWorld(vIn.objectPosition);
    result.clipPosition = TransformWorldToHClip(positionWS);
    
    result.color = vIn.color;
    result.baseUVs = vIn.baseUVs;

    result.pigmentCtrlColor = float3(vIn.pigmentRG.xy, vIn.pigmBsubsB.x);
    result.substrateCtrlColor = float3(vIn.substrateRG.xy, vIn.pigmBsubsB.y);
    result.edgeCtrlColor = float3(vIn.edgeRG.xy, vIn.edgeBabstB.x);
    result.abstractCtrlColor = float3(vIn.abstractRG.xy, vIn.edgeBabstB.y);

    return result;
}

struct MultiRenderTarget
{
    // COMMON DEFFERED RENDERING TARGETS
    // color value from the mesh.color attribute or the material set base color for example
    float4 color : SV_Target0;
    // diffuse value
    float4 diffuse : SV_Target1;
    // Specular
    float4 specular : SV_Target2; // carefull the specular wanted here is not
    // the specular value of the object rendered but the specular of the light 
    // already computed from the specular value, the normals and the lightsource 
    // TODO : Check back this on MNPR, not sure about this claim anymore... if it is true it could be 
    // done between a more common defered rendering and the NPR post process
    // The depth value is already done by the renderBuffer.drawRenderer() command
    

    // UNPR SPECIFIC RENDERING TARGETS
    // Pigment Control Target
    float3 pigmentCtrl : SV_Target3;
    // Substrate Control Target
    float3 substrateCtrl : SV_Target4;
    // Edge Control Target
    float3 edgeCtrl : SV_Target5;
    // Abstraction Control Target
    float3 abstractCtrl : SV_Target6;
};
//fragment
MultiRenderTarget UNPROpaqueFragment(VertexOut fIn)
{
    MultiRenderTarget result;
    //return _BaseColor;
    result.color = _BaseColor * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, fIn.baseUVs);
    result.diffuse = float4(1, 1, 1, 1);//TODO Compute the actual shader
    result.specular = float4(0, 0, 0, 0);

    result.pigmentCtrl = fIn.pigmentCtrlColor;
    result.substrateCtrl = fIn.substrateCtrlColor;
    result.edgeCtrl = fIn.edgeCtrlColor;
    result.abstractCtrl = fIn.abstractCtrlColor;
    return result;
}

#endif /*UNPR_OPAQUE_PASS_INCLUDED*/