////////////////////////////////////////////////////////////////////////////////////////////////////
// Common.hlsl (HLSL)
// Brief: Common utility shader elements for MNPR
// Contributors: Santiago Montesdeoca
////////////////////////////////////////////////////////////////////////////////////////////////////
//                          _
//     __ _ _   _  __ _  __| |       ___ ___  _ __ ___  _ __ ___   ___  _ __
//    / _` | | | |/ _` |/ _` |_____ / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \
//   | (_| | |_| | (_| | (_| |_____| (_| (_) | | | | | | | | | | | (_) | | | |
//    \__, |\__,_|\__,_|\__,_|      \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|
//       |_|
////////////////////////////////////////////////////////////////////////////////////////////////////
// This shader file provides utility variables, structs, vertex shader and functions to aid
// the development of quad operations in MNPR
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _COMMON_HLSL
#define _COMMON_HLSL


#define TEXTURE2D_SAMPLER2D(_Tex, sampler_Tex)\
	TEXTURE2D(_Tex);\
	SAMPLER(sampler_Tex);

// COMMON UNITY VARIABLES
//static float4x4 _WVP = UNITY_MATRIX_MVP;        // world-view-projection transformation // TODO verify if really equals to MAYA's WorldViewProjection + Isn't recognised by unity??? + really necessary???
static float4 _ScreenSize4 = _ScreenParams;  
static float2 _ScreenSize = _ScreenSize4.xy;    // screen size, in pixels


// COMMON VARIABLES
static float3 luminanceCoeff = float3(0.241, 0.691, 0.068);
static float2 _Texel = 1.0f / _ScreenSize;
static const float _PI = 3.14159265f;


// COMMON TEXTURES
//TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);      // MainTexture

TEXTURE2D(_MainTex);\
SAMPLER(sampler_MainTex);
static float4 _MainTex_TexelSize;


// COMMON SAMPLERS 
uniform SamplerState _Sampler;



//        _                   _
//    ___| |_ _ __ _   _  ___| |_ ___
//   / __| __| '__| | | |/ __| __/ __|
//   \__ \ |_| |  | |_| | (__| |_\__ \
//   |___/\__|_|   \__,_|\___|\__|___/
//
// base input structs
struct appData {
	float3 vertex : POSITION;
};
struct appDataSampler {
	float3 vertex : POSITION;
	float2 texcoord : TEXCOORD0;
};
struct vertexOutput {
	float4 pos : SV_POSITION;
};
struct vertexOutputSampler {
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};


/*
//                   _                    _               _
//   __   _____ _ __| |_ _____  __    ___| |__   __ _  __| | ___ _ __ ___
//   \ \ / / _ \ '__| __/ _ \ \/ /   / __| '_ \ / _` |/ _` |/ _ \ '__/ __|
//    \ V /  __/ |  | ||  __/>  <    \__ \ | | | (_| | (_| |  __/ |  \__ \
//     \_/ \___|_|   \__\___/_/\_\   |___/_| |_|\__,_|\__,_|\___|_|  |___/
//
// VERTEX SHADER
vertexOutput quadVert(appData v) {
	vertexOutput o;
	o.pos = mul(float4(v.vertex, 1.0f), _WVP);
	return o;
}

// VERTEX SHADER (with uvs)
vertexOutputSampler quadVertSampler(appDataSampler v) {
	vertexOutputSampler o;
	o.pos = mul(float4(v.vertex, 1.0f), _WVP);
	o.uv = v.texcoord;
	return o;
}
*/


//     __                  _   _
//    / _|_   _ _ __   ___| |_(_) ___  _ __  ___
//   | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
//   |  _| |_| | | | | (__| |_| | (_) | | | \__ \
//   |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
//
float luminance(float3 color) {
	return dot(color.rgb, luminanceCoeff);
}

float4 unpremultiply(float4 color) {
	if (color.a) {
		color.rgb /= color.a;
	}
	return color;
}

#endif /* _COMMON_HLSL */
