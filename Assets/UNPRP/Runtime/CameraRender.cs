using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public partial class CameraRenderer
{
	// Gloabal renderer variables
	ScriptableRenderContext context;
	Camera camera;
	const string bufferName = "Render Camera"; //TODO recheck utiliy of using a custom name (frame debugger information display)...
	CommandBuffer buffer = new CommandBuffer { name = bufferName };

    public UNPRPParameters parameters;
    // Variables mainly used to rework the parameter values
    float renderScale;
    int renderWidth;
    int renderHeight;
    RenderTextureFormat renderFormat;
    float substrateScale;
    float substrateDistortion;
    float edgeDarkeningIntensity;
    float edgeDarkeningWidth;
    float gapsOverlapsWidth;
    float[] bleedingWeights = new float[161]; //max = [-80 ; 80]
    int bleedingRadius;
    float time, previousTime = 0;
    float substrateUpdate;
    Vector2 substrateOffset;
    float pigmentDensity;
    float drybrushThreshold;
    Texture substrateTexture;


    ///////////////////////////////////////////////////////

    // Culling variables
    CullingResults cullingResults;

    ///////////////////////////////////////////////////////
    
	// Rendering/drawing variables
	static ShaderTagId unlitShaderTagId = new ShaderTagId("SRPDefaultUnlit"); // TODO update this to refuse default unlit shaders + add refused to error shader
																			  // Render Targets IDs (render textures linked to them with buffer.GetTemporaryRT() at runtime)
	static int depthTarget          = Shader.PropertyToID("_DepthTex");
	static int colorTarget          = Shader.PropertyToID("_ColorTex");
	static int diffuseTarget        = Shader.PropertyToID("_DiffuseTex");
	static int specularTarget       = Shader.PropertyToID("_SpecularTex");
	//static int velocityTarget		= Shader.PropertyToID("_VelocityTex"); // not used in MNPR (maybe in MNPRX?), so not used here either
	static int pigmentCtrlTarget    = Shader.PropertyToID("_PigmentCtrlTex");
	static int substrateCtrlTarget  = Shader.PropertyToID("_SubstrateCtrlTex");
	static int edgeCtrlTarget       = Shader.PropertyToID("_EdgeCtrlTex");
	static int abstractCtrlTarget   = Shader.PropertyToID("_AbstractCtrlTex");
	int[] colorRenderTargets = { colorTarget, diffuseTarget, specularTarget, /*velocityTarget,*/ pigmentCtrlTarget, substrateCtrlTarget, edgeCtrlTarget, abstractCtrlTarget };
	RenderTargetIdentifier[] colorRenderTargetsIDs = { colorTarget, diffuseTarget, specularTarget, /*velocityTarget,*/ pigmentCtrlTarget, substrateCtrlTarget, edgeCtrlTarget, abstractCtrlTarget };

    ///////////////////////////////////////////////////////

    // Post processing variables

    Material postProcessMaterial; 
    Material postProcessUtilitiesMaterial; 

    // PP Render Textures IDs (render textures linked to them with buffer.GetTemporaryRT() at runtime)
    static int substrateTarget      = Shader.PropertyToID("_SubstrateTargetTex");
	static int edgeTarget           = Shader.PropertyToID("_EdgeTex");
	static int bleedingTarget       = Shader.PropertyToID("_BleedingTex");
	static int stylizationTarget    = Shader.PropertyToID("_StylizationTex");
	static int outputTarget         = Shader.PropertyToID("_OutputTex");
	static int debugOutputTarget    = Shader.PropertyToID("_DebugOutputTarget");
    Dictionary<Camera, RenderTexture> linearDepths = new Dictionary<Camera, RenderTexture>();
    RenderTexture linearDepth; //this texture need to be used by the next frame so it isn't temporary. An ID (linearDepthID) is associated to it once it has been defined

    // Temporary Render textures used to get the output of a texture being modified in a pass and feed it to the pass that'll need it(a render texture cannot be read and written in the same pass)
    // If debug parameters are visible, the result they hold is copied to the original one to access it easily (may reduce performances)
    int stylizationTarget2 = Shader.PropertyToID("_StylizationTex2");
    int tmpTarget1 = Shader.PropertyToID("_TmpTex1");
    int tmpTarget2 = Shader.PropertyToID("_TmpTex2");
    int tmpDepthTarget = Shader.PropertyToID("_tmpDepthTarget"); // a RT to replace depthTarget during MRT as it is already used as an input


    public void Render(ScriptableRenderContext context, Camera camera, UNPRPParameters parameters)
	{
		this.context = context;
		this.camera = camera;
		this.parameters = parameters;

		PrepareBuffer();
		Setup();
		PrepareForSceneWindow(); // Only defined if UNITY_EDITOR in CameraRender.Editor

		// Culling
		if (!Cull())
			return;

		// Rendering (or Drawing)
		DrawVisibleGeometry();
		
		// Post-Processing
		PostProcess();


        /* test end
        buffer.Blit(outputTarget, BuiltinRenderTextureType.CameraTarget);
        ExecuteBuffer();
        buffer.ReleaseTemporaryRT(outputTarget);

        buffer.ReleaseTemporaryRT(depthTarget);
        foreach (int renderTargetID in colorRenderTargets)
			buffer.ReleaseTemporaryRT(renderTargetID);//*/

        DrawUnsupportedShaders(); // Only defined if UNITY_EDITOR or DEVELOPMENT_BUILD in CameraRender.Editor
		DrawGizmos(); // Only defined if UNITY_EDITOR in CameraRender.Editor
        buffer.ReleaseTemporaryRT(depthTarget);
        Submit();
	}

	void Setup() //TODO choose betwen this setup and the beginning of draw geom func
	{
		context.SetupCameraProperties(camera);
		//buffer.ClearRenderTarget(true, true, Color.clear);
		buffer.BeginSample(SampleName);
		ExecuteBuffer();

        
        //if (camera.depthTextureMode != DepthTextureMode.DepthNormals)
        //    camera.depthTextureMode = DepthTextureMode.DepthNormals;

        // REWORKING PARAMETER VALUES : // TODO is it the best place for this kind of calculations? (here it is calculated at each frame...)
        // Matching some parameters with the scale
        renderScale = parameters.renderScale;
        renderWidth = (int)(camera.pixelWidth * renderScale);
        renderHeight = (int)(camera.pixelHeight * renderScale);
        renderFormat = (RenderTextureFormat)parameters.colorDepth;
        substrateScale = parameters.substrateScale;
        substrateDistortion = parameters.substrateDistortion * renderScale;

        edgeDarkeningIntensity = parameters.edgeDarkeningIntensity * renderScale;
        edgeDarkeningWidth = (float)Math.Round(parameters.edgeDarkeningWidth * renderScale);
        gapsOverlapsWidth = (float)Math.Round(parameters.maxGapsOverlapsWidth * renderScale);

        // setting the bleeding wheights // TODO else this isn't working or it is when these are transmitted to the shader.. 
        if (bleedingRadius != parameters.bleedingRadius)
        {
            bleedingRadius = parameters.bleedingRadius;
            float sigma = (float)bleedingRadius * 2.0f;
            // calculate new bleeding kernel
            float normDivisor = 0;
            for (int x = -bleedingRadius; x <= bleedingRadius; x++)
            {
                float weight = (float)(0.15915 * Math.Exp(-0.5 * x * x / (sigma * sigma)) / sigma);
                //float weight = (float)(pow((6.283185*sigma*sigma), -0.5) * exp((-0.5*x*x) / (sigma*sigma)));
                normDivisor += weight;
                bleedingWeights[x + bleedingRadius] = weight;
            }
            // normalize weights and make gaussian wheights
            for (int x = -bleedingRadius; x <= bleedingRadius; x++)
            {
                bleedingWeights[x + bleedingRadius] /= normDivisor;
            }
        }
        // Avoiding a null texture
        substrateTexture = parameters.substrateTexture ?? Texture2D.whiteTexture;

        if (parameters.substrateUpdate != 0)
        {
            substrateUpdate = 1.0f / parameters.substrateUpdate; // Time between each substrate update
            time = Time.time;
            if (time - previousTime >= substrateUpdate)
            {
                //Renderer.material.SetTextureOffset("_substrateTexture", new Vector2((float)(Math.Sin(time) * 0.5453),(float)(Math.Sin(time) * 0.8317)));
                substrateOffset = new Vector2((float)(Math.Sin(time) * 0.5453), (float)(Math.Sin(time) * 0.8317));
                previousTime = time;
            }
        }

        pigmentDensity = parameters.pigmentDensity;// not really usefull //TODO ?
        drybrushThreshold = (float)20.0 - parameters.drybrushThreeshold;


        linearDepth = null;
        // Non temporary targets (with informations from previous frame) 
        bool keyExist = linearDepths.TryGetValue(camera, out linearDepth);
        if (!keyExist || linearDepth == null || !linearDepth.IsCreated() || linearDepth.width != renderWidth || linearDepth.height != renderHeight)
        {
            //TODO this is a dirty fix ideally unused camera should destroy their associated linearDepth texture when destroyed/unloaded
            if (keyExist && linearDepth == null)
                linearDepths.Remove(camera);

            //if there has not been any previous frame yet (the render texture isn't created) or if the size of the render has changed, the target is reconstructed
            if (linearDepth != null)
            {
                linearDepth.Release();
                UnityEngine.Object.DestroyImmediate(linearDepth);
                linearDepths.Remove(camera); 
            }
            linearDepth = new RenderTexture(renderWidth, renderHeight, 0, renderFormat, RenderTextureReadWrite.Linear);
            linearDepth.Create();
            linearDepths.Add(camera, linearDepth);
        }
        //var linearDepthID = new RenderTargetIdentifier(linearDepth); // an ID to access this render texture as all the other temporary render texture

        if (m_blitTri == null)
            m_blitTri = simpleTri();
    }

    bool Cull()
	{
		if (camera.TryGetCullingParameters(out ScriptableCullingParameters p))
		{
			cullingResults = context.Cull(ref p);
			return true;
		}
		return false;
	}

	void DrawVisibleGeometry()
    {
		// Setup render targets
		// initialising RT
		buffer.GetTemporaryRT(depthTarget, renderWidth, renderHeight, 16, FilterMode.Point, RenderTextureFormat.Depth, RenderTextureReadWrite.Default); //TODO check needs for RenderTextureReadWrite
		foreach (int renderTargetID in colorRenderTargets)
			buffer.GetTemporaryRT(renderTargetID, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Default); //TODO check needs for RenderTextureReadWrite
		buffer.SetRenderTarget( colorRenderTargetsIDs, depthTarget);
		buffer.ClearRenderTarget(true, true, Color.clear);
		ExecuteBuffer();

		//draw opaque
		SortingSettings sortingSettings = new SortingSettings(camera) { criteria = SortingCriteria.CommonOpaque };
		DrawingSettings drawingSettings = new DrawingSettings(unlitShaderTagId, sortingSettings);
		FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque);

		context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings);

		//draw skybox
		context.DrawSkybox(camera);

		//draw transparent
		sortingSettings.criteria = SortingCriteria.CommonTransparent;
		drawingSettings.sortingSettings = sortingSettings;
		filteringSettings.renderQueueRange = RenderQueueRange.transparent;

		context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings);
	}

    void PostProcess()
    {
        if (postProcessMaterial == null)
        {
            Shader testShader = Shader.Find("Hidden/UNPR/PostProcess");
            postProcessMaterial = new Material(testShader) { hideFlags = HideFlags.HideAndDontSave };
        }

        if (postProcessUtilitiesMaterial == null)
        {
            Shader testShader = Shader.Find("Hidden/UNPR/PostProcessUtilities");
            postProcessUtilitiesMaterial = new Material(testShader) { hideFlags = HideFlags.HideAndDontSave };
        }


        //buffer.BeginSample("NPR Post Process Rendering");

        // If the debug parameter are shown all the render textures are created before the post process begin and are freed once every pass are done
        // this gives the possibility to easily check the content of each renderTarget but uses more ressources
        if (parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(outputTarget, renderWidth, renderHeight, 0, FilterMode.Bilinear, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(stylizationTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(stylizationTarget2, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(substrateTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(edgeTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(bleedingTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(tmpTarget1, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(tmpTarget2, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);

            buffer.GetTemporaryRT(tmpDepthTarget, renderWidth, renderHeight, 16, FilterMode.Point, RenderTextureFormat.Depth, RenderTextureReadWrite.Linear); // a RT to replace depthTarget during MRT as it is already used as an input

            buffer.GetTemporaryRT(debugOutputTarget, renderWidth, renderHeight, 0, FilterMode.Bilinear, renderFormat, RenderTextureReadWrite.Linear);//only used in debug mode

            //initialize newly created RTs to black (as they might not be rendered to if the pass is not selected)
            BlitCopy(Texture2D.blackTexture, outputTarget);
            BlitCopy(Texture2D.blackTexture, stylizationTarget);
            BlitCopy(Texture2D.blackTexture, stylizationTarget2);
            BlitCopy(Texture2D.blackTexture, substrateTarget);
            BlitCopy(Texture2D.blackTexture, edgeTarget);
            BlitCopy(Texture2D.blackTexture, bleedingTarget);
            BlitCopy(Texture2D.blackTexture, tmpTarget1);
            BlitCopy(Texture2D.blackTexture, tmpTarget2);
        }

        // BEGINNING RENDERING

        // Setting the main textures that'll need to be accessed as parameter in the shader(s) as global 
        // TODO send to shader with setParameter if possible : no need for all the shaders to access it!
        buffer.SetGlobalTexture("_DepthTex", depthTarget);
        //buffer.SetGlobalTexture("_VelocityTex", velocityTarget);
        buffer.SetGlobalTexture("_DiffuseTex", diffuseTarget);
        buffer.SetGlobalTexture("_SpecularTex", specularTarget);
        buffer.SetGlobalTexture("_PigmentCtrlTex", pigmentCtrlTarget);
        buffer.SetGlobalTexture("_SubstrateCtrlTex", substrateCtrlTarget);
        buffer.SetGlobalTexture("_EdgeCtrlTex", edgeCtrlTarget);
        buffer.SetGlobalTexture("_AbstractCtrlTex", abstractCtrlTarget);
        buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);

        // If a target has to be shown and will not be redrawn later, it is blitted/copyied to destination now // TODO these kind of switch can be better? with returns?
        
        if (parameters.showDebugParam == true)
        {
            switch ((int)parameters.activeTarget)
            {
                case (int)UNPRPParameters.ActiveTarget.colorTarget:
                    BlitCopy(colorTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.depthTarget:
                    BlitCopy(depthTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.diffuseTarget:
                    BlitCopy(diffuseTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.specularTarget:
                    BlitCopy(specularTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.pigmentCtrlTarget:
                    BlitCopy(pigmentCtrlTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.substrateCtrlTarget:
                    BlitCopy(substrateCtrlTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.edgeCtrlTarget:
                    BlitCopy(edgeCtrlTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.abstractCtrlTarget:
                    BlitCopy(abstractCtrlTarget, debugOutputTarget);
                    break;
            }
        }






        //       _       _  _           _        _                    _ 
        //      / \   __| |(_)_   _ ___| |_     | |    ___   __ _  __| |
        //     / _ \ / _` || | | | / __| __|____| |   / _ \ / _` |/ _` |
        //    / ___ \ (_| || | |_| \__ \ ||_____| |__| (_) | (_| | (_| |
        //   /_/   \_\__,_|/ |\__,_|___/\__|    |_____\___/ \__,_|\__,_|
        //               |__/                                               

        // NEW RENDER TEXTURES
        // Declaring the new render targets necessary from or for this step and setting them as global for the ones that'll need to be accessed as parameter in the shader(s)
        if (!parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(substrateTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(stylizationTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(tmpTarget1, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(tmpTarget2, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
            buffer.GetTemporaryRT(tmpDepthTarget, renderWidth, renderHeight, 16, FilterMode.Point, RenderTextureFormat.Depth, RenderTextureReadWrite.Linear); // a RT to replace depthTarget during MRT as it is already used as an input
            //buffer.Blit(context.source, tmpDepthTarget, postProcessUtilitiesMaterial, (int)UNPRPParameters.UtilityPass._MainTexCopy); // UNPRPPSv2 remains : This line isn't really necessary it seems
        }

        // PARAMETERS
        // Transmitting the necessary parameters to the shader
        postProcessMaterial.SetVector("_DepthRange", parameters.depthRange);
        postProcessMaterial.SetFloat("_Saturation", parameters.saturation);
        postProcessMaterial.SetFloat("_Contrast", parameters.contrast);
        postProcessMaterial.SetFloat("_Brightness", parameters.brightness);
        postProcessMaterial.SetColor("_SubstrateColor", parameters.substrateColor);
        postProcessMaterial.SetColor("_AtmosphereTint", parameters.atmosphereTint);
        postProcessMaterial.SetVector("_AtmosphereRange", parameters.atmosphereRange);
        postProcessMaterial.SetTexture("_SubstrateTex", substrateTexture);
        postProcessMaterial.SetFloat("_SubstrateRoughness", parameters.substrateRoughness);
        postProcessMaterial.SetFloat("_SubstrateScale", substrateScale);
        postProcessMaterial.SetVector("_SubstrateOffset", substrateOffset); // originally automatically send with the texture but I could not manage to change the offset of the texture in this script so I'm sending another variable...

        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);
        //buffer.SetGlobalTexture("_DepthTex", depthTarget);
        //buffer.SetGlobalTexture("_VelocityTex", velocityTarget);
        //buffer.SetGlobalTexture("_DiffuseTex", diffuseTarget);
        //buffer.SetGlobalTexture("_SpecularTex", specularTarget); 

        // Parameter comming from inside unity ? TODO redirect from accessible PPparameters
        postProcessMaterial.SetFloat("_Gamma", 1.0f); // TODO find true origin and replace
        postProcessMaterial.SetFloat("_EnableVelocityPV", 0.0f); // TODO find true origin and replace
        postProcessMaterial.SetFloat("_NearClipPlane", camera.nearClipPlane); // TODO suppress as not necessary?

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Adjust-load pass (with Multi Rendering Target)
            RenderTargetIdentifier[] RenderTargets = { stylizationTarget, substrateTarget, tmpTarget1, tmpTarget2 };
            //                            to replace {stylizationTarget, substrateTarget, linearDepthTarget, velocityTarget}

            Blit(colorTarget, RenderTargets, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.AdjustLoad);

            buffer.CopyTexture(tmpTarget1, linearDepth);
            // TODO with the implementation of velocity, tmpTarget2 will need to be:
            // - used to convey the velocityTarget to the next pass were it is needed 
            // - copied to the actual velocityTarget
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.AdjustLoad])
            {
                // Adjust-load pass (with Multi Rendering Target)
                RenderTargetIdentifier[] RenderTargets = { stylizationTarget, substrateTarget, tmpTarget1, tmpTarget2 };
                //                            to replace {stylizationTarget, substrateTarget, linearDepthTarget, velocityTarget}

                //buffer.Blit(colorTarget, RenderTargets, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.AdjustLoad);
                Blit(colorTarget, RenderTargets, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.AdjustLoad);

                buffer.CopyTexture(tmpTarget1, linearDepth);

                //buffer.CopyTexture(tmpTarget2, velocityTarget);
            }

            // If a target has to be shown and will not be redrawn later, it is blitted/copyied to destination now
            switch ((int)parameters.activeTarget)
            {
                case (int)UNPRPParameters.ActiveTarget.substrateTarget:
                    BlitCopy(substrateTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.linearDepth:
                    BlitCopy(linearDepth, debugOutputTarget);
                    break;
                //case (int)UNPRPParameters.ActiveTarget.velocityTarget: // TODO after implementation, move this command after last writing of velocity
                //    BlitCopy(velocityTarget, debugOutputTarget);
                //    break;
            }
        }

        // RENDER TEXTURE RELEASE
        // Release the Render Texture that are not necesarry anymore
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(colorTarget);
            buffer.ReleaseTemporaryRT(diffuseTarget);
            buffer.ReleaseTemporaryRT(specularTarget);
            //buffer.ReleaseTemporaryRT(velocityTarget); // this is where velocity stop being used, just after being initialized, without having a use (yet) in NPR -> TODO after implementation, move this command after last use of velocity
        }







        //    _____    _              ____       _            _   _             
        //   | ____|__| | __ _  ___  |  _ \  ___| |_ ___  ___| |_(_) ___  _ __  
        //   |  _| / _` |/ _` |/ _ \ | | | |/ _ \ __/ _ \/ __| __| |/ _ \| '_ \ 
        //   | |__| (_| | (_| |  __/ | |_| |  __/ ||  __/ (__| |_| | (_) | | | |
        //   |_____\__,_|\__, |\___| |____/ \___|\__\___|\___|\__|_|\___/|_| |_|
        //               |___/                                                  

        // NEW RENDER TEXTURES
        // Declaring the new render targets necessary from this step and setting them as global for the ones that'll need to be accessed as parameter in the shader(s)
        if (!parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(edgeTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
        }

        // PARAMETERS
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Edge detection pass
            Blit(stylizationTarget, edgeTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.EdgeDetection);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.EdgeDetection])
            {
                // Edge detection pass
                Blit(stylizationTarget, edgeTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.EdgeDetection);
            }
        }

        // RENDER TEXTURE RELEASE





        //    ____  _                            _     ____                 _ _         
        //   |  _ \(_) __ _ _ __ ___   ___ _ __ | |_  |  _ \  ___ _ __  ___(_) |_ _   _ 
        //   | |_) | |/ _` | '_ ` _ \ / _ \ '_ \| __| | | | |/ _ \ '_ \/ __| | __| | | |
        //   |  __/| | (_| | | | | | |  __/ | | | |_  | |_| |  __/ | | \__ \ | |_| |_| |
        //   |_|   |_|\__, |_| |_| |_|\___|_| |_|\__| |____/ \___|_| |_|___/_|\__|\__, |
        //            |___/                                                       |___/ 

        // NEW RENDER TEXTURES
        if (!parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(stylizationTarget2, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
        }

        // PARAMETERS
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_PigmentCtrlTex", pigmentCtrlTarget);
        //postProcessMaterial.SetColor("_SubstrateColor", PPparameters.substrateColor);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Pigment Density pass
            Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.PigmentDensity);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.PigmentDensity])
            {
                // Pigment Density pass
                Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.PigmentDensity);

                buffer.CopyTexture(stylizationTarget2, stylizationTarget);
            }
        }

        // RENDER TEXTURE RELEASE



        //    ____                             _     _        _   _ 
        //   / ___|  ___ _ __   __ _ _ __ __ _| |__ | | ___  | | | |
        //   \___ \ / _ \ '_ \ / _` | '__/ _` | '_ \| |/ _ \ | |_| |
        //    ___) |  __/ |_) | (_| | | | (_| | |_) | |  __/ |  _  |
        //   |____/ \___| .__/ \__,_|_|  \__,_|_.__/|_|\___| |_| |_|
        //              |_|                                         

        // NEW RENDER TEXTURES
        if (!parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(bleedingTarget, renderWidth, renderHeight, 0, FilterMode.Point, renderFormat, RenderTextureReadWrite.Linear);
        }

        // PARAMETERS
        postProcessMaterial.SetFloat("_RenderScale", renderScale);
        postProcessMaterial.SetFloat("_BleedingThreeshold", parameters.bleedingThreeshold);
        postProcessMaterial.SetFloat("_EdgeDarkeningWidth", parameters.edgeDarkeningWidth);
        postProcessMaterial.SetFloat("_GapsOverlapsWidth", parameters.maxGapsOverlapsWidth);
        postProcessMaterial.SetInt("_BleedingRadius", bleedingRadius);
        postProcessMaterial.SetFloatArray("_BleedingWeights", bleedingWeights);
        buffer.SetGlobalTexture("_EdgeTex", edgeTarget);
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_EdgeCtrlTex", edgeCtrlTarget);
        //buffer.SetGlobalTexture("_AbstractCtrlTex", abstractCtrlTarget);
        //buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);


        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Separable H pass (Multi Render Target)
            RenderTargetIdentifier[] RenderTargets2 = { tmpTarget1, tmpTarget2 };
            //                             to replace {bleedingTarget, edgeTarget}

            //buffer.Blit(stylizationTarget2, RenderTargets2, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableH);
            Blit(stylizationTarget2, RenderTargets2, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableH);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.SeparableH])
            {
                // Separable H pass (Multi Render Target)
                RenderTargetIdentifier[] RenderTargets2 = { tmpTarget1, tmpTarget2 };
                //                             to replace {bleedingTarget, edgeTarget}

                //buffer.Blit(stylizationTarget2, RenderTargets2, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableH);
                Blit(stylizationTarget2, RenderTargets2, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableH);

                buffer.CopyTexture(tmpTarget1, bleedingTarget);
                buffer.CopyTexture(tmpTarget2, edgeTarget);
            }
        }

        // RENDER TEXTURE RELEASE





        //    ____                             _     _       __     __
        //   / ___|  ___ _ __   __ _ _ __ __ _| |__ | | ___  \ \   / /
        //   \___ \ / _ \ '_ \ / _` | '__/ _` | '_ \| |/ _ \  \ \ / / 
        //    ___) |  __/ |_) | (_| | | | (_| | |_) | |  __/   \ V /  
        //   |____/ \___| .__/ \__,_|_|  \__,_|_.__/|_|\___|    \_/   
        //              |_|                                           

        // NEW RENDER TEXTURES


        // PARAMETERS
        buffer.SetGlobalTexture("_EdgeTex", tmpTarget2); // the _EdgeTex parameter is changed to tmpTarget1 as this is the  ones that hold its latest definition
                                                         // Parameter used here but already transmited or set as global before:
                                                         //buffer.SetGlobalTexture("_EdgeCtrlTex", edgeCtrlTarget);
                                                         //buffer.SetGlobalTexture("_AbstractCtrlTex", abstractCtrlTarget);
                                                         //buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);
                                                         //postProcessMaterial.SetFloat("_RenderScale", renderScale);
                                                         //postProcessMaterial.SetFloat("_BleedingThreeshold", PPparameters.bleedingThreeshold);
                                                         //postProcessMaterial.SetFloat("_EdgeDarkenningWidth", PPparameters.edgeDarkeningWidth);
                                                         //postProcessMaterial.SetFloat("_GapsOverlapsWidth", PPparameters.maxGapsOverlapsWidth);
                                                         //postProcessMaterial.SetInt("_BleedingRadius", bleedingRadius);
                                                         //postProcessMaterial.SetFloatArray("_BleedingWeights", bleedingWeights);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Separable V pass (Multi Render Target)
            RenderTargetIdentifier[] RenderTargets3 = { bleedingTarget, edgeTarget };

            //buffer.Blit(tmpTarget1, RenderTargets3, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableV);
            Blit(tmpTarget1, RenderTargets3, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableV);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.SeparableV])
            {
                if (!parameters.pass[(int)UNPRPParameters.Pass.SeparableH])
                {
                    // if the previous pass has not been made, the tmpTargets are empty...
                    buffer.CopyTexture(edgeTarget, tmpTarget2);
                    buffer.CopyTexture(bleedingTarget, tmpTarget1);
                }
                // Separable V pass (Multi Render Target)
                RenderTargetIdentifier[] RenderTargets3 = { bleedingTarget, edgeTarget };

                //buffer.Blit(tmpTarget1, RenderTargets3, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableV);
                Blit(tmpTarget1, RenderTargets3, tmpDepthTarget, postProcessMaterial, (int)UNPRPParameters.Pass.SeparableV);
            }

            // If a target has to be shown and will not be redrawn later, it is blitted/copyied to destination now
            switch ((int)parameters.activeTarget)
            {
                case (int)UNPRPParameters.ActiveTarget.edgeTarget:
                    BlitCopy(edgeTarget, debugOutputTarget);
                    break;
                case (int)UNPRPParameters.ActiveTarget.bleedingTarget:
                    BlitCopy(bleedingTarget, debugOutputTarget);
                    break;
            }
        }


        // RENDER TEXTURE RELEASE
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(abstractCtrlTarget);
            buffer.ReleaseTemporaryRT(tmpDepthTarget);
            buffer.ReleaseTemporaryRT(tmpTarget2);
        }






        //    ____  _               _ _             
        //   | __ )| | ___  ___  __| (_)_ __   __ _ 
        //   |  _ \| |/ _ \/ _ \/ _` | | '_ \ / _` |
        //   | |_) | |  __/  __/ (_| | | | | | (_| |
        //   |____/|_|\___|\___|\__,_|_|_| |_|\__, |
        //                                    |___/ 

        // NEW RENDER TEXTURES

        // PARAMETERS
        buffer.SetGlobalTexture("_BleedingTex", bleedingTarget);
        // Parameter used here but already transmited or set as global before:

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Bleeding pass
            Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.Bleeding);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.Bleeding])
            {
                // Bleeding pass
                Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.Bleeding);
            }
        }

        // RENDER TEXTURE RELEASE





        //    _____    _              ____             _                    _             
        //   | ____|__| | __ _  ___  |  _ \  __ _ _ __| | _____ _ __  _ __ (_)_ __   __ _ 
        //   |  _| / _` |/ _` |/ _ \ | | | |/ _` | '__| |/ / _ \ '_ \| '_ \| | '_ \ / _` |
        //   | |__| (_| | (_| |  __/ | |_| | (_| | |  |   <  __/ | | | | | | | | | | (_| |
        //   |_____\__,_|\__, |\___| |____/ \__,_|_|  |_|\_\___|_| |_|_| |_|_|_| |_|\__, |
        //               |___/                                                      |___/ 

        // NEW RENDER TEXTURES

        // PARAMETERS
        buffer.SetGlobalTexture("_EdgeTex", edgeTarget); // the real Edge target is set back as the _EdgeTex parameter
        postProcessMaterial.SetFloat("_EdgeDarkenningIntensity", edgeDarkeningIntensity);
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_EdgeCtrlTex", edgeCtrlTarget);
        //postProcessMaterial.SetColor("_SubstrateColor", PPparameters.substrateColor);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Edge Darkenning Pass
            Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.EdgeDarkenning);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.EdgeDarkenning])
            {
                // Edge Darkenning Pass
                Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.EdgeDarkenning);

                buffer.CopyTexture(stylizationTarget2, stylizationTarget);
            }
        }

        // RENDER TEXTURE RELEASE





        //     ____                    ___      ___                 _                 
        //    / ___| __ _ _ __  ___   ( _ )    / _ \__   _____ _ __| | __ _ _ __  ___ 
        //   | |  _ / _` | '_ \/ __|  / _ \/\ | | | \ \ / / _ \ '__| |/ _` | '_ \/ __|
        //   | |_| | (_| | |_) \__ \ | (_>  < | |_| |\ V /  __/ |  | | (_| | |_) \__ \
        //    \____|\__,_| .__/|___/  \___/\/  \___/  \_/ \___|_|  |_|\__,_| .__/|___/
        //               |_|                                               |_|        

        // NEW RENDER TEXTURES

        // PARAMETERS
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_EdgeTex", edgeTarget);
        //buffer.SetGlobalTexture("_EdgeCtrlTex", edgeCtrlTarget);
        //buffer.SetGlobalTexture("_BleedingTex", bleedingTarget);
        //postProcessMaterial.SetColor("_SubstrateColor", PPparameters.substrateColor);
        //postProcessMaterial.SetFloat("_GapsOverlapsWidth", PPparameters.maxGapsOverlapsWidth);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Gaps And Overlaps Pass
            Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.GapsAndOverlaps);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.GapsAndOverlaps])
            {
                // Gaps And Overlaps Pass
                Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.GapsAndOverlaps);
            }
        }


        // RENDER TEXTURE RELEASE
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(edgeCtrlTarget);
            buffer.ReleaseTemporaryRT(edgeTarget);
            buffer.ReleaseTemporaryRT(bleedingTarget);
        }






        //    ____  _                            _        _                _ _           _   _             
        //   |  _ \(_) __ _ _ __ ___   ___ _ __ | |_     / \   _ __  _ __ | (_) ___ __ _| |_(_) ___  _ __  
        //   | |_) | |/ _` | '_ ` _ \ / _ \ '_ \| __|   / _ \ | '_ \| '_ \| | |/ __/ _` | __| |/ _ \| '_ \ 
        //   |  __/| | (_| | | | | | |  __/ | | | |_   / ___ \| |_) | |_) | | | (_| (_| | |_| | (_) | | | |
        //   |_|   |_|\__, |_| |_| |_|\___|_| |_|\__| /_/   \_\ .__/| .__/|_|_|\___\__,_|\__|_|\___/|_| |_|
        //            |___/                                   |_|   |_|                                    

        // NEW RENDER TEXTURES

        // PARAMETERS
        postProcessMaterial.SetFloat("_PigmentDensity", parameters.pigmentDensity);
        postProcessMaterial.SetFloat("_DryBrushThreshold", drybrushThreshold);
        buffer.SetGlobalTexture("_SubstrateTargetTex", substrateTarget);
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_PigmentCtrlTex", pigmentCtrlTarget);
        //postProcessMaterial.SetColor("_SubstrateColor", PPparameters.substrateColor);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Pigment Application Pass
            Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.PigmentApplication);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.PigmentApplication])
            {
                // Pigment Application Pass
                Blit(stylizationTarget, stylizationTarget2, null, postProcessMaterial, (int)UNPRPParameters.Pass.PigmentApplication);

                buffer.CopyTexture(stylizationTarget2, stylizationTarget);
            }
        }

        // RENDER TEXTURE RELEASE
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(pigmentCtrlTarget);
        }






        //    ____        _         _             _         ____  _     _             _   _             
        //   / ___| _   _| |__  ___| |_ _ __ __ _| |_ ___  |  _ \(_)___| |_ ___  _ __| |_(_) ___  _ __  
        //   \___ \| | | | '_ \/ __| __| '__/ _` | __/ _ \ | | | | / __| __/ _ \| '__| __| |/ _ \| '_ \ 
        //    ___) | |_| | |_) \__ \ |_| | | (_| | ||  __/ | |_| | \__ \ || (_) | |  | |_| | (_) | | | |
        //   |____/ \__,_|_.__/|___/\__|_|  \__,_|\__\___| |____/|_|___/\__\___/|_|   \__|_|\___/|_| |_|
        //                                                                                              

        // NEW RENDER TEXTURES

        // PARAMETERS
        postProcessMaterial.SetFloat("_SubstrateDistortion", substrateDistortion);
        // Parameter used here but already transmited or set as global before:
        //buffer.SetGlobalTexture("_LinearDepthTex", linearDepth);
        //buffer.SetGlobalTexture("_SubstrateCtrlTex", substrateCtrlTarget);
        //buffer.SetGlobalTexture("_SubstrateTargetTex", substrateTarget);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Substrate Distortion Pass
            Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.SubstrateDistortion);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.SubstrateDistortion])
            {
                // Substrate Distortion Pass
                Blit(stylizationTarget2, stylizationTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.SubstrateDistortion);
            }

            // If a target has to be shown and will not be redrawn later, it is blitted/copyied to destination now
            switch ((int)parameters.activeTarget)
            {
                case (int)UNPRPParameters.ActiveTarget.stylizationTarget:
                    BlitCopy(stylizationTarget, debugOutputTarget);
                    break;
            }
        }


        // RENDER TEXTURE RELEASE
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(substrateCtrlTarget);
            buffer.ReleaseTemporaryRT(stylizationTarget2);
        }





        //       _          _   _       _ _           _             
        //      / \   _ __ | |_(_) __ _| (_) __ _ ___(_)_ __   __ _ 
        //     / _ \ | '_ \| __| |/ _` | | |/ _` / __| | '_ \ / _` |
        //    / ___ \| | | | |_| | (_| | | | (_| \__ \ | | | | (_| |
        //   /_/   \_\_| |_|\__|_|\__,_|_|_|\__,_|___/_|_| |_|\__, |
        //                                                    |___/ 

        // NEW RENDER TEXTURES
        if (!parameters.showDebugParam)
        {
            buffer.GetTemporaryRT(outputTarget, renderWidth, renderHeight, 0, FilterMode.Bilinear, renderFormat, RenderTextureReadWrite.Linear);
        }

        // PARAMETERS
        postProcessMaterial.SetFloat("_AntiAliasing", (float)parameters.antialiasing);
        // Parameter used here but already transmited or set as global before:
        //postProcessMaterial.SetFloat("_RenderScale", renderScale);

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // AntiAliasing Pass
            Blit(stylizationTarget, tmpTarget1, null, postProcessMaterial, (int)UNPRPParameters.Pass.Antialiasing);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.Antialiasing])
            {
                // AntiAliasing Pass
                Blit(stylizationTarget, tmpTarget1, null, postProcessMaterial, (int)UNPRPParameters.Pass.Antialiasing);
                buffer.CopyTexture(tmpTarget1, outputTarget);
            }
        }

        // RENDER TEXTURE RELEASE
        if (!parameters.showDebugParam)
        {
            buffer.ReleaseTemporaryRT(stylizationTarget);
        }





        //    ____        _         _             _         _     _       _     _         _             
        //   / ___| _   _| |__  ___| |_ _ __ __ _| |_ ___  | |   (_) __ _| |__ | |_ _ __ (_)_ __   __ _ 
        //   \___ \| | | | '_ \/ __| __| '__/ _` | __/ _ \ | |   | |/ _` | '_ \| __| '_ \| | '_ \ / _` |
        //    ___) | |_| | |_) \__ \ |_| | | (_| | ||  __/ | |___| | (_| | | | | |_| | | | | | | | (_| |
        //   |____/ \__,_|_.__/|___/\__|_|  \__,_|\__\___| |_____|_|\__, |_| |_|\__|_| |_|_|_| |_|\__, |
        //                                                          |___/                         |___/ 

        // NEW RENDER TEXTURES

        // PARAMETERS
        postProcessMaterial.SetFloat("_SubstrateLightDir", parameters.substrateLightDir);
        postProcessMaterial.SetFloat("_SubstrateLightTilt", parameters.substrateLightTilt);
        postProcessMaterial.SetFloat("_SubstrateShading", parameters.substrateShading);
        // Parameter used here but already transmited or set as global before:
        //postProcessMaterial.SetFloat("_Gamma", PPparameters.gamma); // TODO find true origin and replace

        // PASS X CUSTOM OUTPUT
        if (parameters.showDebugParam == false)
        {
            // Substrate Lightning Pass
            Blit(tmpTarget1, outputTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.SubstrateLightning);
        }
        else
        {
            if (parameters.pass[(int)UNPRPParameters.Pass.SubstrateLightning])
            {
                // Substrate Lightning Pass
                Blit(tmpTarget1, outputTarget, null, postProcessMaterial, (int)UNPRPParameters.Pass.SubstrateLightning);
            }

            // If a target has to be shown and will not be redrawn, it is blitted/copyied to destination now //TODO needless if the render textures are only freed at the end...
            switch ((int)parameters.activeTarget)
            {
                case (int)UNPRPParameters.ActiveTarget.outputTarget:
                    buffer.CopyTexture(outputTarget, debugOutputTarget);
                    break;
            }
        }



        // RENDER TEXTURE RELEASE
        // Releasing the Temporary render texture for them to be used by next getTemporaryRT
        if (!parameters.showDebugParam)
        {
            buffer.Blit(outputTarget, BuiltinRenderTextureType.CameraTarget);//TODO replace with custom BlitCopy method need a flip correct for UVs
            //BlitCopy(outputTarget, BuiltinRenderTextureType.CameraTarget);

            buffer.ReleaseTemporaryRT(substrateTarget);
            buffer.ReleaseTemporaryRT(outputTarget);
            buffer.ReleaseTemporaryRT(tmpTarget1);
        }


        if (parameters.showDebugParam)
        {
            buffer.Blit(debugOutputTarget, BuiltinRenderTextureType.CameraTarget);//TODO replace with custom BlitCopy method need a flip correct for UVs
            //BlitCopy(debugOutputTarget, BuiltinRenderTextureType.CameraTarget);

            //rendering RTs
            buffer.ReleaseTemporaryRT(colorTarget);
            buffer.ReleaseTemporaryRT(diffuseTarget);
            buffer.ReleaseTemporaryRT(specularTarget);
            buffer.ReleaseTemporaryRT(pigmentCtrlTarget);
            buffer.ReleaseTemporaryRT(edgeCtrlTarget);
            buffer.ReleaseTemporaryRT(substrateCtrlTarget);
            buffer.ReleaseTemporaryRT(abstractCtrlTarget);

            //PostProcess RTs
            buffer.ReleaseTemporaryRT(outputTarget);
            buffer.ReleaseTemporaryRT(stylizationTarget);
            buffer.ReleaseTemporaryRT(stylizationTarget2);
            buffer.ReleaseTemporaryRT(substrateTarget);
            buffer.ReleaseTemporaryRT(bleedingTarget);
            buffer.ReleaseTemporaryRT(edgeTarget);
            buffer.ReleaseTemporaryRT(tmpTarget1);
            buffer.ReleaseTemporaryRT(tmpTarget2);

            buffer.ReleaseTemporaryRT(debugOutputTarget);

            buffer.ReleaseTemporaryRT(tmpDepthTarget);
        }

        //buffer.EndSample("NPR Post Process Rendering");


        //buffer.BeginSample("postProcess");// TODO : Check why doesn't this appear in the frame debugger


        //buffer.EndSample("postProcess");


        ExecuteBuffer();
    }

	void Submit()
	{
		buffer.EndSample(SampleName);
		ExecuteBuffer();
		context.Submit();
	}

	void ExecuteBuffer()
    {
		context.ExecuteCommandBuffer(buffer);
		buffer.Clear();
    }
}