using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class UNPRP : RenderPipeline
{
    UNPRPParameters parameters;
    CameraRenderer renderer;

    public float test = 0f;

    public UNPRP(UNPRPParameters parameters)
    {
        this.parameters = parameters;
        GraphicsSettings.useScriptableRenderPipelineBatching = true;
        renderer = new CameraRenderer() { parameters = parameters };
    }

    protected override void Render (ScriptableRenderContext context, Camera[] cameras)
    {
        foreach (Camera camera in cameras)
        {
            renderer.Render(context, camera, parameters);
        }
    }
}
