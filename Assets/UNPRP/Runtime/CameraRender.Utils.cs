using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public partial class CameraRenderer
{
	enum CopyType
    {
		color = 0,
		depth = 1,
		copyflip = 2 //copy the texture with a flipped matrix to counter UVStarts at top if necessary
    };

	Mesh m_blitTri;

	/* Mesh simpleQuad(float height, float width)
    {
		Mesh mesh = new Mesh();

		Vector3[] vertices = new Vector3[4]
		{
			new Vector3(0, 0, 0),
			new Vector3(width, 0, 0),
			new Vector3(0, height, 0),
			new Vector3(width, height, 0)
		};
		mesh.vertices = vertices;

		int[] tris = new int[6]
		{
            // lower left triangle
            0, 2, 1,
            // upper right triangle
            2, 3, 1
		};
		mesh.triangles = tris;

		Vector3[] normals = new Vector3[4]
		{
			-Vector3.forward,
			-Vector3.forward,
			-Vector3.forward,
			-Vector3.forward
		};
		mesh.normals = normals;

		Vector2[] uv = new Vector2[4]
		{
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1)
		};
		mesh.uv = uv;

		return mesh;
	}*/

	static Mesh simpleTri() // makes a single triangle  to render RenderTexture to.
					 // (necessary because cmdbuffer.Blit() isn't compatible with multi render target so we use a custom cmdbuffer.drawMesh() instead)
    {
		Mesh mesh = new Mesh();
		Vector3[] vertices = new Vector3[3]
		{
			new Vector3(-1, -1, 0),
			new Vector3(-1, 3, 0),
			new Vector3(3, -1, 0)
		};
		mesh.vertices = vertices;

		int[] tris = new int[3]
		{
            0, 1, 2
		};
		mesh.triangles = tris;

		Vector3[] normals = new Vector3[]
		{
			-Vector3.forward,
			-Vector3.forward,
			-Vector3.forward
		};
		mesh.normals = normals;

		Vector2[] uv = new Vector2[3]
		{
#if UNITY_UV_STARTS_AT_TOP //inverts Y component of UV if necessary
			new Vector2(0, 0),
			new Vector2(0, 2),
			new Vector2(2, 0)
#else
			new Vector2(0, 1),
			new Vector2(0, -1),
			new Vector2(2, 1)
#endif
		};
		mesh.uv = uv;

		return mesh;
	}

	//Custom blit (with support of Multi Render Targets)

	/// <summary>
	/// simply copy a renderTarget/Texture to another one
	/// </summary>
	/// <param name="mainColorIn"></param>
	/// <param name="colorOut"></param>
	void BlitCopy(RenderTargetIdentifier mainColorIn, RenderTargetIdentifier colorOut, CopyType type = CopyType.color) //if needed to add specific pass for depth / blitflip
    {
		Blit(mainColorIn, colorOut, null, postProcessUtilitiesMaterial, (int)type);
	}

	void Blit(RenderTargetIdentifier? mainColorIn, RenderTargetIdentifier colorOut, RenderTargetIdentifier? depthOut, Material material, int pass)
    {
		Blit(mainColorIn, new RenderTargetIdentifier[] { colorOut }, depthOut, material, pass);
    }
	void Blit(RenderTargetIdentifier? mainColorIn, RenderTargetIdentifier[] colorsOut, RenderTargetIdentifier? depthOut, Material material, int pass)
	{
		if (colorsOut.Length <= 0)
        {
			Debug.LogError("No RenderTarget given for blit! colorsOut array is empty!");
			return;
        }

		if (mainColorIn != null)
			buffer.SetGlobalTexture("_MainTex", (RenderTargetIdentifier)mainColorIn); //TODO update to setTexture instead?

		if (depthOut != null || colorsOut.Length > 1) // we set the depth target too if one has been specified or if we are using MRTs (there is no version of SetRenderTarget which permit MRTs without using depthTarget)
			buffer.SetRenderTarget(colorsOut, depthOut ?? colorsOut[0]);
		else
			buffer.SetRenderTarget(colorsOut[0]);

		buffer.DrawMesh(m_blitTri, Matrix4x4.identity, material, 0, pass);
	}
}