using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UNPRPParameters
{
    // The name of the differents pass coded in the shader file
    public enum Pass
    {
        AdjustLoad,
        EdgeDetection,
        PigmentDensity,
        SeparableH,
        SeparableV,
        Bleeding,
        EdgeDarkenning,
        GapsAndOverlaps,
        PigmentApplication,
        SubstrateDistortion,
        Antialiasing,
        SubstrateLightning
    }

    public enum UtilityPass
    {
        _MainTexCopy,
        _CameraDepthTextureCopy,
        _VelocityCopy
    }

    public enum ActiveTarget
    {
        colorTarget,
        depthTarget,
        diffuseTarget,
        specularTarget,
        pigmentCtrlTarget,
        substrateCtrlTarget,
        edgeCtrlTarget,
        abstractCtrlTarget,
        substrateTarget,
        linearDepth,
        edgeTarget,
        velocityTarget,
        bleedingTarget,
        stylizationTarget,
        outputTarget
    }

    public enum Style
    {
        Watercolor,
        Charcoal_NOTIMPLEMENTED
    }

    public enum AntiAliasingQuality
    {
        None = 0,
        FXAA = 1,
        FXAA_high = 2
    }

    public enum ColorDepth
    {
        //TODO find a fancier way for these value (similar to the render scale parameter?)
        _8bit = RenderTextureFormat.ARGB32,
        _16bit = RenderTextureFormat.ARGBHalf, // or ARGB64 ?
        _32bit = RenderTextureFormat.ARGBFloat // or ARGBInt ?
    }

    public Style style;
    public ColorDepth colorDepth; //TODO
    public float renderScale;
    public AntiAliasingQuality antialiasing;
    public Vector2 depthRange;
    public Color atmosphereTint;
    public Vector2 atmosphereRange;
    public bool showDebugParam;


    public float pigmentDensity;
    public float edgeDarkeningIntensity;
    public int edgeDarkeningWidth;
    public int bleedingRadius;
    public float bleedingThreeshold;
    public float drybrushThreeshold;
    public int maxGapsOverlapsWidth;


    public Texture substrateTexture;
    public Color substrateColor;
    public float substrateShading;
    public float substrateLightDir;
    public float substrateLightTilt;
    public float substrateScale;
    public float substrateUpdate;
    public float substrateRoughness;
    public float substrateDistortion;


    public float saturation;
    public float contrast;
    public float brightness;


    public ActiveTarget activeTarget;
    public bool[] pass = new bool[12];
}
