using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

public partial class CameraRenderer
{
	//rendering of UI for the scene camera 
	partial void PrepareForSceneWindow();
#if UNITY_EDITOR
	partial void PrepareForSceneWindow()
	{
		if (camera.cameraType == CameraType.SceneView)
		{
			ScriptableRenderContext.EmitWorldGeometryForSceneView(camera);
		}
	}
#endif

	//Gizmos rendering
	partial void DrawGizmos();
#if UNITY_EDITOR
	partial void DrawGizmos()
	{
		if (Handles.ShouldRenderGizmos())
		{
			// to reset the depth buffer after post process MRT with tmpDepthTarget we need this next line
			// (SetRenderTarget does not set the depth target to be read from apparently, only written to)
			BlitCopy(depthTarget, BuiltinRenderTextureType.CameraTarget, CopyType.depth);

			buffer.SetRenderTarget(BuiltinRenderTextureType.CameraTarget, depthTarget);
			ExecuteBuffer();
			context.DrawGizmos(camera, GizmoSubset.PreImageEffects);
			context.DrawGizmos(camera, GizmoSubset.PostImageEffects);
		}
	}
#endif

	//set specific sample name for each camera
	partial void PrepareBuffer();

#if UNITY_EDITOR || DEVELOPMENT_BUILD
	string SampleName { get; set; }
	
	partial void PrepareBuffer()
	{
		Profiler.BeginSample("Editor/Development Build  Only");
		buffer.name = SampleName = camera.name;
		Profiler.EndSample();
	}
#else
	const string SampleName = bufferName;
#endif

	//Unsupported Shader rendering
	partial void DrawUnsupportedShaders();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
	static ShaderTagId[] legacyShaderTagIds = {
		new ShaderTagId("Always"),
		new ShaderTagId("ForwardBase"),
		new ShaderTagId("PrepassBase"),
		new ShaderTagId("Vertex"),
		new ShaderTagId("VertexLMRGBM"),
		new ShaderTagId("VertexLM")
	};
	static Material errorMaterial;

	partial void DrawUnsupportedShaders()
	{
		if (errorMaterial == null)
		{
			errorMaterial =
				new Material(Shader.Find("Hidden/InternalErrorShader"));
		}

		var drawingSettings = new DrawingSettings(legacyShaderTagIds[0], new SortingSettings(camera)){overrideMaterial = errorMaterial};
		for (int i = 1; i < legacyShaderTagIds.Length; i++)
			drawingSettings.SetShaderPassName(i, legacyShaderTagIds[i]);

		FilteringSettings filteringSettings = FilteringSettings.defaultValue;

		context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings);
	}	
#endif

}