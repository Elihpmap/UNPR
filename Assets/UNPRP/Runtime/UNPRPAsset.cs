using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(menuName = "Rendering/UNPRP")]
public class UNPRPAsset : RenderPipelineAsset 
{
    [SerializeField]
    public UNPRPParameters parameters;
    protected override RenderPipeline CreatePipeline()
    {
        return new UNPRP(parameters);
    }
}
