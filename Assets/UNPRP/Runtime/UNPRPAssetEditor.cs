using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UNPRPAsset))]
internal sealed class MyPipelineAssetEditor : Editor
{
    SerializedProperty m_Style;
    SerializedProperty m_ShowDebugParams;
    SerializedProperty m_ColorDepth;
    SerializedProperty m_RenderScale;
    SerializedProperty m_AntiAliasing;
    SerializedProperty m_DepthRange;
    SerializedProperty m_AtmosphereTint;
    SerializedProperty m_AtmosphereRange;


    SerializedProperty m_PigmentDensity;
    SerializedProperty m_EdgeDarkeningIntensity;
    SerializedProperty m_EdgeDarkeningWidth;
    SerializedProperty m_BleedingRadius;
    SerializedProperty m_BleedingThreeshold;
    SerializedProperty m_DrybrushThreeshold;
    SerializedProperty m_MaxGapsOverlapsWidth;


    SerializedProperty m_SubstrateTexture;
    SerializedProperty m_SubstrateColor;
    SerializedProperty m_SubstrateShading;
    SerializedProperty m_SubstrateLightDir;
    SerializedProperty m_SubstrateLightTilt;
    SerializedProperty m_SubstrateScale;
    SerializedProperty m_SubstrateUpdate;
    SerializedProperty m_SubstrateRoughness;
    SerializedProperty m_SubstrateDistortion;


    SerializedProperty m_Saturation;
    SerializedProperty m_Contrast;
    SerializedProperty m_Brightness;


    SerializedProperty m_ActiveTarget;
    SerializedProperty passArraySize;
    SerializedProperty m_Pass;


    bool isdown = false;


    void OnEnable()
    {
        m_Style = serializedObject.FindProperty("parameters").FindPropertyRelative("style");
        m_ShowDebugParams = serializedObject.FindProperty("parameters").FindPropertyRelative("showDebugParam");
        m_ColorDepth = serializedObject.FindProperty("parameters").FindPropertyRelative("colorDepth");
        m_RenderScale = serializedObject.FindProperty("parameters").FindPropertyRelative("renderScale");
        m_AntiAliasing = serializedObject.FindProperty("parameters").FindPropertyRelative("antialiasing");
        m_DepthRange = serializedObject.FindProperty("parameters").FindPropertyRelative("depthRange");
        m_AtmosphereTint = serializedObject.FindProperty("parameters").FindPropertyRelative("atmosphereTint");
        m_AtmosphereRange = serializedObject.FindProperty("parameters").FindPropertyRelative("atmosphereRange");


        m_PigmentDensity = serializedObject.FindProperty("parameters").FindPropertyRelative("pigmentDensity");
        m_EdgeDarkeningIntensity = serializedObject.FindProperty("parameters").FindPropertyRelative("edgeDarkeningIntensity");
        m_EdgeDarkeningWidth = serializedObject.FindProperty("parameters").FindPropertyRelative("edgeDarkeningWidth");
        m_BleedingRadius = serializedObject.FindProperty("parameters").FindPropertyRelative("bleedingRadius");
        m_BleedingThreeshold = serializedObject.FindProperty("parameters").FindPropertyRelative("bleedingThreeshold");
        m_DrybrushThreeshold = serializedObject.FindProperty("parameters").FindPropertyRelative("drybrushThreeshold");
        m_MaxGapsOverlapsWidth = serializedObject.FindProperty("parameters").FindPropertyRelative("maxGapsOverlapsWidth");


        m_SubstrateTexture = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateTexture");
        m_SubstrateColor = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateColor");
        m_SubstrateShading = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateShading");
        m_SubstrateLightDir = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateLightDir");
        m_SubstrateLightTilt = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateLightTilt");
        m_SubstrateScale = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateScale");
        m_SubstrateUpdate = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateUpdate");
        m_SubstrateRoughness = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateRoughness");
        m_SubstrateDistortion = serializedObject.FindProperty("parameters").FindPropertyRelative("substrateDistortion");


        m_Saturation = serializedObject.FindProperty("parameters").FindPropertyRelative("saturation");
        m_Contrast = serializedObject.FindProperty("parameters").FindPropertyRelative("contrast");
        m_Brightness = serializedObject.FindProperty("parameters").FindPropertyRelative("brightness");
        

        m_ActiveTarget = serializedObject.FindProperty("parameters").FindPropertyRelative("activeTarget");
        m_Pass = serializedObject.FindProperty("parameters").FindPropertyRelative("pass");
        passArraySize = m_Pass.FindPropertyRelative("Array.size");


        //TODO set defaultValues? save values
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        EditorGUILayout.HelpBox("This Post Processing effect is still in development and may not work yet.", MessageType.Warning);


        if (m_ShowDebugParams.boolValue == true)
        {
            EditorGUILayout.HelpBox("With debug parameters on, some texture are copied at each frame to be displayed easily, which may reduce FPS", MessageType.Warning);

            EditorGUILayout.LabelField("Rendering Insight", UnityEditor.EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(m_ActiveTarget);
            EditorGUI.indentLevel++;
            isdown = EditorGUILayout.Foldout(isdown, "Pass");
            if (isdown)
            {
                string passName;

                for (int i = 0; i < passArraySize.intValue; i++)
                {
                    passName = ((UNPRPParameters.Pass)i).ToString();
                    EditorGUILayout.PropertyField(m_Pass.GetArrayElementAtIndex(i), new GUIContent(passName, "Activate/deactivate " + passName));
                }
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider); // a separation line
        }
        else
        {
            // If they're not shown, their values can't be changed from default 
            m_ActiveTarget.intValue = (int)UNPRPParameters.ActiveTarget.outputTarget;
            // No need to impact the Passes, if debug parameters are off, they're always all executed
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Engine Settings", UnityEditor.EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(m_Style);
        EditorGUILayout.PropertyField(m_ShowDebugParams);
        EditorGUILayout.PropertyField(m_ColorDepth);
        EditorGUILayout.PropertyField(m_RenderScale);
        // block the possible value to 0.5, 1 or 2
        float renderScale = m_RenderScale.floatValue;
        if (renderScale != 1f && renderScale != 0.5f && renderScale != 2f)
        {
            if (renderScale < 0.75f)
            {
                m_RenderScale.floatValue = 0.5f;
            }
            else if (renderScale > 1.5f)
            {
                m_RenderScale.floatValue = 2f;
            }
            else
            {
                m_RenderScale.floatValue = 1f;
            }
        }
        EditorGUILayout.PropertyField(m_AntiAliasing);
        EditorGUILayout.PropertyField(m_DepthRange); // TODO check with far clip plane? + avoid impossible vallue (min being greater than max)
        EditorGUILayout.PropertyField(m_AtmosphereTint);
        EditorGUILayout.PropertyField(m_AtmosphereRange); // TODO check with far clip plane? + avoid impossible vallue (min being greater than max)

        // We choose which menu to display depending on the style choice 
        if (m_Style.intValue == (int)UNPRPParameters.Style.Watercolor)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Watercolor", UnityEditor.EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(m_PigmentDensity);
            EditorGUILayout.PropertyField(m_EdgeDarkeningIntensity);
            EditorGUILayout.PropertyField(m_EdgeDarkeningWidth);
            EditorGUILayout.PropertyField(m_BleedingRadius);
            EditorGUILayout.PropertyField(m_BleedingThreeshold);
            EditorGUILayout.PropertyField(m_DrybrushThreeshold);
            EditorGUILayout.PropertyField(m_MaxGapsOverlapsWidth);
        }
        else if (m_Style.intValue == (int)UNPRPParameters.Style.Charcoal_NOTIMPLEMENTED)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Charcoal", UnityEditor.EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Charcoal Renderring is not implemented yet", MessageType.Error);
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Substrate", UnityEditor.EditorStyles.boldLabel);
        EditorGUILayout.ObjectField(m_SubstrateTexture, typeof(Texture2D));
        if (m_SubstrateTexture.objectReferenceValue == null)
        {
            // If no texture has been chosen, it is replaced in the UNPRPostProcess script, and the information is displayed here
            EditorGUILayout.HelpBox("No texture has been chosen, the substrate will be calculated on a white texture", MessageType.Info);
        }
        EditorGUILayout.PropertyField(m_SubstrateColor);
        EditorGUILayout.PropertyField(m_SubstrateShading);
        EditorGUILayout.PropertyField(m_SubstrateLightDir);
        EditorGUILayout.PropertyField(m_SubstrateLightTilt);
        EditorGUILayout.PropertyField(m_SubstrateScale);
        EditorGUILayout.PropertyField(m_SubstrateUpdate);
        EditorGUILayout.PropertyField(m_SubstrateRoughness);
        EditorGUILayout.PropertyField(m_SubstrateDistortion);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Post-processing", UnityEditor.EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(m_Saturation);
        EditorGUILayout.PropertyField(m_Contrast);
        EditorGUILayout.PropertyField(m_Brightness);

        serializedObject.ApplyModifiedProperties();
    }

}
