using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A rework of the SelectByName tool made for 51 B.A.F.F.E.S ...
/// </summary>
public class SelectObjectByComponent : ScriptableWizard
{
    [Tooltip("The component name to search : if this component is found, the corresponding GameObject will be in the selection\n" +
             " - Leave it blank to not select any object.")]
    public string toSearch = "";
    [Tooltip("The component to avoid : if this component is found, the corresponding GameObject will be deselected.\n" +
             " - Leave it blank to not deselect any object.")]
    public string toAvoid = "";
    [Tooltip("Limit the search to the children of this GameObject.\n" +
             " - Leave empty to search the entire active scene.")]
    public GameObject limitToChildren;
    [Tooltip("If activated, debug messages will be printed")]
    public bool debugOn = false;

    [MenuItem("Custom Tools/Select by Component")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<SelectObjectByComponent>("Select GameObjects by Component", "Select GameObjects", "Reselect on current selection");
    }

    GameObject[] getCorrespondingGameObjectsIn(GameObject[] gameObjectsTable)
    {
        List<GameObject> toSelect = new List<GameObject> { };

        foreach (GameObject gameObject in gameObjectsTable)
        {
            bool isValid;

            if (toSearch != "")
            {
                isValid = gameObject.GetComponent(toSearch) != null;

                if (debugOn)
                {
                Debug.Log("Searched \"" + toSearch + "\" in \"" + gameObject.name + "\" and found :" + isValid);
                }
            }
            else
            {
                isValid = true;
            }

            if (toAvoid != "" && isValid)
            {
                isValid = gameObject.GetComponent(toAvoid) == null;

                if (debugOn)
                {
                    Debug.Log("Searched \"" + toAvoid + "\" in \"" + gameObject.name + "\" and found :" + !isValid);
                }
            }

            if (isValid)
            {
                toSelect.Add(gameObject);
            }
        }

        return toSelect.ToArray();
    }

    bool checkValues(bool isLaunching)
    {
        if (isLaunching)
        {
            if (toSearch == "" && toAvoid == "")
            {
                errorString = "No filter detected, please make sure to enter usefull values!";
                return false;
            }
        }

        return true;
    }

    void OnWizardCreate()
    {
        if (checkValues(true))
        {
            GameObject[] rootGameObjects;
            List<GameObject> gameObjectsList = new List<GameObject> { };

            if (limitToChildren != null)
            {
                rootGameObjects = new GameObject[] { limitToChildren };
            }
            else
            {
                rootGameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            }

            foreach (GameObject gameObject in rootGameObjects)
            {
                foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>(true))
                {
                    gameObjectsList.Add(transform.gameObject);
                }
                gameObjectsList.Add(gameObject);
            }

            Selection.objects = getCorrespondingGameObjectsIn(gameObjectsList.ToArray());
        }
        else
        {
            Debug.LogError("SelectObjectByComponent : Invalid value found, selection unchanged...");
        }
    }

    void OnWizardOtherButton()
    {
        if (checkValues(true))
        {
            if (limitToChildren != null)
            {
                errorString = "This function cannot be limited to a single gameObject and its children, " +
                    "please leave the corresponding value blank or use the \"Select GameObjects\" button";
            }
            else
            {
                Selection.objects = getCorrespondingGameObjectsIn(Selection.gameObjects);
            }
        }
    }

    void OnWizardUpdate()
    {
        if (checkValues(false))
        {
            errorString = "";
        }
    }

}