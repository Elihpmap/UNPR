using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A tool made for 51 B.A.F.F.E.S ...
/// </summary>
public class SelectObjectByName : ScriptableWizard
{
    [Tooltip("The string to search : if this string is found in a name, the corresponding GameObject will be in the selection\n" +
             " - Leave it blank to select all game objects.")]
    public string toSearch = "";
    [Tooltip("The interval of the names to search (in number of character) : if the toSearch string is not in this interval, it will not be selected.\n" +
             " - Leave a value to 0 to set no beginning or end limit,\n" +
             " - Negative values will be counted from the end of the names.")]
    public Vector2Int rangeToSearch = new Vector2Int(0, 0);
    [Tooltip("The string to avoid : if this string is found in a name, the corresponding GameObject will be deselected.\n" +
             " - Leave it blank to not deselect any object.")]
    public string toAvoid = "";
    [Tooltip("The interval of the names on which the toAvoid string will be searched (in number of character) : if the toAvoid string is not found in this interval it will not have any influence.\n" +
             " - Leave a value to 0 to set no beginning or end limit,\n" +
             " - Negative values will be counted from the end of the names.")]
    public Vector2Int rangeToAvoid = new Vector2Int(0, 0);
    [Tooltip("Limit the search to the children of this GameObject.\n" +
             " - Leave empty to search the entire active scene.")]
    public GameObject limitToChildren;
    [Tooltip("If activated, The case will be ignored (Example : \"Hello Dear\" == \"hello dear\" -> True)")]
    public bool ignoreCase = false;
    [Tooltip("If activated, debug messages will be printed")]
    public bool debugOn = false;

    [MenuItem("Custom Tools/Select by Name")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<SelectObjectByName>("Select GameObjects by name", "Select GameObjects", "Reselect on current selection");
    }

    GameObject[] getCorrespondingGameObjectsIn(GameObject[] gameObjectsTable)
    {
        List<GameObject> toSelect = new List<GameObject> { };

        if (ignoreCase)
        {
            toSearch = toSearch.ToLower();
            toAvoid = toAvoid.ToLower();
        }

        foreach (GameObject gameObject in gameObjectsTable)
        {
            string name = gameObject.name;
            bool isValid;

            if (toSearch != "")
            {
                Vector2Int relativeRangetoSearch = getRelativeRange(rangeToSearch, name.Length); // relative range is (startIndex, length)
                if (relativeRangetoSearch.y >= toSearch.Length)
                {
                    string searchString = (relativeRangetoSearch.y == 0) ? "" : name.Substring(relativeRangetoSearch.x, relativeRangetoSearch.y);
                    searchString = ignoreCase ? searchString.ToLower() : searchString;

                    isValid = searchString.Contains(toSearch);

                    if (debugOn)
                    {
                        Debug.Log("Searched \"" + toSearch + "\" in \"" + searchString + "\" and found :" + isValid);
                    }
                }
                else
                {
                    isValid = false;
                    if (debugOn)
                    {
                        Debug.Log("Skipped search for \"" + toSearch + "\" in the gameObject named \"" + name + "\" because relativeRange is to small : " + relativeRangetoSearch);
                    }
                }
            }
            else
            {
                isValid = true;
            }

            if (toAvoid != "" && isValid)
            {
                Vector2Int relativeRangetoAvoid = getRelativeRange(rangeToAvoid, name.Length); // relative range is (startIndex, length);
                if (relativeRangetoAvoid.y >= toAvoid.Length)
                {
                    string avoidString = (relativeRangetoAvoid.y == 0) ? "" : name.Substring(relativeRangetoAvoid.x, relativeRangetoAvoid.y);
                    avoidString = ignoreCase ? avoidString.ToLower() : avoidString;

                    isValid = !avoidString.Contains(toAvoid);

                    if (debugOn)
                    {
                        Debug.Log("Searched for\"" + toAvoid + "\" in \"" + avoidString + "\" and found : " + !isValid);
                    }
                }
                else if (debugOn)
                {
                    Debug.Log("Skipped search for \"" + toAvoid + "\" in the gameObject named \"" + name + "\" because relativeRange is to small : " + relativeRangetoAvoid);
                }
            }

            if (isValid)
            {
                toSelect.Add(gameObject);
            }
        }

        return toSelect.ToArray();
    }

    bool checkValues(bool isLaunching)
    {
        if ((rangeToSearch.x == rangeToSearch.y && rangeToSearch.x != 0) ||
            (rangeToSearch.x < 0 && rangeToSearch.y < 0 && rangeToSearch.x > rangeToSearch.y) ||
            (rangeToSearch.x > 0 && rangeToSearch.y > 0 && rangeToSearch.x > rangeToSearch.y))
        {
            errorString = "rangeToSearch has an invalid value! Leave a value to 0 to set no beginning or end limit.";
            return false;
        }

        if ((rangeToAvoid.x == rangeToAvoid.y && rangeToAvoid.x != 0) ||
            (rangeToAvoid.x < 0 && rangeToAvoid.y < 0 && rangeToAvoid.x > rangeToAvoid.y) ||
            (rangeToAvoid.x > 0 && rangeToAvoid.y > 0 && rangeToAvoid.x > rangeToAvoid.y))
        {
            errorString = "rangeToAvoid has an invalid value! Leave a value to 0 to set no beginning or end limit.";
            return false;
        }

        if (isLaunching)
        {
            if (toSearch == "" && toAvoid == "")
            {
                errorString = "No filter detected, please make sure to enter usefull values!";
                return false;
            }
        }

        return true;
    }

    Vector2Int getRelativeRange(Vector2Int initRange, int maxSize) // Relative range is (startIndex, length)
    {
        Vector2Int relativeRange = new Vector2Int();
        int upLimit;

        relativeRange.x = (initRange.x < 0) ? (maxSize + initRange.x) : initRange.x;
        relativeRange.x = relativeRange.x < 0 ? 0 : relativeRange.x;

        if (initRange.y == 0 || initRange.y > maxSize)
            upLimit = maxSize;
        else if (initRange.y < 0)
            upLimit = maxSize + initRange.y;
        else
            upLimit = initRange.y;

        relativeRange.y = (relativeRange.x >= upLimit) ? 0 : upLimit - relativeRange.x;

        if (debugOn)
        {
            Debug.Log("Returned relativeRange" + relativeRange + "\n" +
                "Calculated with initRange : " + initRange + ", maxSize : " + maxSize + ", and found relative upLimit : " + upLimit);
        }

        return relativeRange;
    }

    void OnWizardCreate()
    {
        if (checkValues(true))
        {
            GameObject[] rootGameObjects;
            List<GameObject> gameObjectsList = new List<GameObject> { };

            if (limitToChildren != null)
            {
                rootGameObjects = new GameObject[] { limitToChildren };
            }
            else
            {
                rootGameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            }

            foreach (GameObject gameObject in rootGameObjects)
            {
                foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>(true))
                {
                    gameObjectsList.Add(transform.gameObject);
                }
                gameObjectsList.Add(gameObject);
            }

            Selection.objects = getCorrespondingGameObjectsIn(gameObjectsList.ToArray());
        }
        else
        {
            Debug.LogError("SelectObjectByname : Invalid value found, selection unchanged...");
        }
    }

    void OnWizardOtherButton()
    {
        if (checkValues(true))
        {
            if (limitToChildren != null)
            {
                errorString = "This function cannot be limited to a single gameObject and its children, please leave the corresponding value blank.";
            }
            else
            {
                Selection.objects = getCorrespondingGameObjectsIn(Selection.gameObjects);
            }
        }
    }

    void OnWizardUpdate()
    {
        if (checkValues(false))
        {
            errorString = "";
        }
    }

}