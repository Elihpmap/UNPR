using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A basic tool to change the UV value for every vertex of a mesh to a single value
/// </summary>
public class ChangeUVTool : ScriptableWizard
{
    //[Tooltip("tooltip here")]
    public Vector2 uv = Vector2.zero;
    public int channel = 0;

    [MenuItem("Custom Tools/change uv")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<ChangeUVTool>("change uv", "change uv", "change uv without closing");
    }

    void OnWizardCreate()
    {
        ChangeUV();
    }
    
    void OnWizardOtherButton()
    {
        ChangeUV();
    }

    void OnWizardUpdate()
    {

    }

    void ChangeUV()
    {
        foreach (UnityEngine.Object objet in Selection.objects)
        {
            Mesh mesh = ((GameObject)objet).GetComponent<MeshFilter>().mesh; //TODO check if there is a cleaner way to do this
            List<Vector2> uvs = new List<Vector2>(mesh.vertexCount);
            for (int i = 0; i < mesh.vertexCount; i++)
                uvs.Add(uv);
            mesh.SetUVs(channel, uvs); //TODO need to instantiate mesh manualy to avoid unity error message -> and thus need to take care of those generated meshes
            Debug.Log("changed uvs of mesh" + mesh);
        }
    }
}